﻿using AswLib;

//presta 16 PCZP5ZMTUM5Y4ZBPHD3XES7AJXNMCZKZ
using Bukimedia.PrestaSharp;
using Bukimedia.PrestaSharp.Entities;
using eshop_sync.Properties;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Mail;

//dominosport WZDVPP6Y80P3VSNURQC2FOM2IA8H28AJ
internal static class Module1
{
    private static string _app = "";
    private static int _cislo_app = 0;
    private static DateTime _dtm;
    private static int _chyb = 0;
    private static string _log = "";
    private static AswLib.Export.Export exp = new AswLib.Export.Export();
    
    public static System.Boolean IsNumeric(System.Object Expression)
    {
        if (Expression == null || Expression is DateTime)
            return false;

        if (Expression is Int16 || Expression is Int32 || Expression is Int64 || Expression is Decimal || Expression is Single || Expression is Double || Expression is Boolean)
            return true;

        try
        {
            if (Expression is string)
                Double.Parse(Expression as string);
            else
                Double.Parse(Expression.ToString());
            return true;
        }
        catch { } // just dismiss errors but return false
        return false;
    }
    // základní program
    public static void Main()
    {
        string[] margs = Environment.GetCommandLineArgs();
        string[] args = new string[margs.Length - 1];
        for (int i = 1; i < margs.Length; i++)
        {
            args[i - 1] = margs[i];
        }

        if (args.Length > 0)
        {
            switch (args[0])
            {
                case "-s":

                    if (args.Length > 1)
                    {
                        int oznameni = 0;
                        if (args.Length > 2)
                        {
                            if (IsNumeric(args[2]))
                            {
                                oznameni = Convert.ToInt32(args[2]);
                            }
                        }
                        else
                        {
                        }

                        _cislo_app = Convert.ToInt32(args[1]);

                        switch (_cislo_app)
                        {
                            case 0:
                                SetChanges(oznameni);
                                break;

                            case 1:
                                AnalyzeCategories(oznameni);
                                break;

                            case 2:
                                AnalyzeProducts(oznameni);
                                break;

                            case 3:
                                UpdateAllProductImage(true, true);
                                break;

                            case 4:
                                SetChanges(oznameni, 14);
                                break;

                            case 5:
                                SetChanges(0, 0, oznameni);
                                break;

                            case 6:
                                SetChanges(oznameni, 0, 0, true, false, Convert.ToInt32(args[3]), Convert.ToInt32(args[4]));
                                break;

                            case 7:
                                AnalyzeCategories(oznameni, false, 0, true);
                                break;
                                                                
                            case 8:
                                ReadAllWebCategories();
                                break;
                                                            
                            case 9:
                                ReadAllWebProducts();
                                break;

                            case 10:
                                ReadAllLanguages();
                                break;

                            case 11:
                                SetNullWebCategories();
                                break;

                            case 12:
                                DeletAllWebCategories();
                                break;

                            case 13:
                                DeleteAllWebProducts();
                                break;
                            case 14:
                                UpdateNoActiveItems();
                                break;

                            case 50:
                                ClearLogs();
                                break;
                                
                            case 100:
                                Example();
                                break;
                        }
                    }

                    break;

                case "-r":

                    if (args.Length > 1)
                    {
                        try
                        {
                            Console.WriteLine(string.Format("Aktuální hodnota je: {0}", Settings.Default[args[1]].ToString()));
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Neznámý příkaz nebo parametr!!!");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Nebyl zadán parametr!!!");
                    }
                    break;

                case "-w":

                    if (args.Length > 2)
                    {
                        try
                        {
                            Settings.Default[args[1]] = args[2];
                            Console.WriteLine(string.Format("Hodnota parametru [{0}] byla aktualizována.", Settings.Default[args[1]].ToString()));
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Neznámý příkaz nebo parametr!!!");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Nebyl zadán parametr nebo hodnota!!!");
                    }
                    break;

                case "-t":

                    Console.Write("Testuji připojení k databázi: ");
                    AswLib.Pripojeni.SQL prip = new AswLib.Pripojeni.SQL(Settings.Default.cnn_priority);
                    prip.Cmd.CommandText = "Select count(id) from products";
                    prip.Cmd.CommandType = CommandType.Text;
                    prip.Exec();
                    if (prip.SQLAno)
                    {
                        Console.WriteLine("OK");
                    }
                    else
                    {
                        Console.WriteLine(string.Format("CHYBA - {0}", prip.Chyba));
                    }
                    prip.Dispose();
                    Console.Write("Testuji připojení k eshopu: ");

                    Bukimedia.PrestaSharp.Factories.ProductFactory fct = new Bukimedia.PrestaSharp.Factories.ProductFactory(Settings.Default.baseurl, Settings.Default.account, "");
                    try
                    {
                        Bukimedia.PrestaSharp.Entities.product myprd = fct.Get(1);
                        Console.WriteLine("OK");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(string.Format("CHYBA - {0}", ex.Message.ToString()));
                    }

                    Console.Write("Testuji SMTP server: ");

                    SmtpClient client = new SmtpClient();
                    System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential(Settings.Default.smtp_user, Settings.Default.smtp_pwd);
                    //Put your own, or your ISPs, mail server name onthis next line
                    client.Host = Settings.Default.smtp_srv;
                    client.Port = Settings.Default.smtp_port;

                    client.UseDefaultCredentials = false;
                    client.Credentials = basicAuthenticationInfo;
                    try
                    {
                        client.Send(Settings.Default.smtp_user, Settings.Default.smtp_user, "TEST", "Testovací eamil eshop_sync");
                        Console.WriteLine("OK");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(string.Format("CHYBA: {0}", ex.Message.ToString()));
                    }

                    break;

                case "-?":
                    Version ver = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                    Console.WriteLine(string.Format("Synchronizace TRANSWARE iNET s eshopem (PrestaShop v 1.6 a výše) - aktuální verze {0}.{1}.{2}.{3}", ver.Major, ver.MajorRevision, ver.Minor, ver.MinorRevision));
                    Console.WriteLine("Microdata spol. s r.o. 2017");
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("eshop_sync.exe -[příkaz] [Parametr] [OPTIONAL VALUE]");
                    Console.WriteLine();
                    Console.WriteLine("? = nápověda");
                    Console.WriteLine("r = výpis hodnoty");
                    Console.WriteLine("w = zápis hodnoty");
                    Console.WriteLine("s = spuštění aplikace");
                    Console.WriteLine("t = test nastavení");
                    Console.WriteLine();
                    Console.WriteLine("Nastavení parametrů aplikace");
                    Console.WriteLine();
                    Console.WriteLine("eshop_sync.exe -[r/w] [Parametr] [OPTIONAL VALUE] ");
                    Console.WriteLine();
                    Console.WriteLine("Parametry:");
                    Console.WriteLine();
                    Console.WriteLine("baseurl           [url eshopu např: http://microdata.cz/eshop/api]");
                    Console.WriteLine("account           [web api key pro přihlášení k eshopu]");
                    Console.WriteLine("default_lng       [výchozí id jazky v eshopu]");
                    Console.WriteLine("cnn               [connection string k připojení k databázi TRANSWARE iNET]");
                    Console.WriteLine("default_catparent [výchozí id root kategorie v eshopu]");
                    Console.WriteLine("smtp_srv          [adresa smtp server pro odesílání emailů]");
                    Console.WriteLine("smtp_user         [email oprávněného uživatele serveru pro odesílání emailů]");
                    Console.WriteLine("smtp_pwd          [heslo oprávněného uživatele serveru pro odesílání emailů]");
                    Console.WriteLine("smtp_port         [port smtp serveru pro odesílání emailů]");
                    Console.WriteLine("senderemail       [email odesíatele]");
                    Console.WriteLine("emaillist         [emailové adresy příjemců oddělené čárkou pro zasílání emailů]");
                    Console.WriteLine();

                    Console.WriteLine("Použtí aplikace");
                    Console.WriteLine();
                    Console.WriteLine("eshop_sync.exe -s [APLIKACE] [EMAIL NOTIFIKACE] ");
                    Console.WriteLine();

                    Console.WriteLine("0 = Synchronizace produktů na základě provedených změn");
                    Console.WriteLine("1 = Synchronizace kategorií v eshopu");
                    Console.WriteLine("2 = Synchronizace produktů a jejich atributů");
                    Console.WriteLine("3 = Synchronizace obrázků produktů a kategorií");
                    Console.WriteLine("4 = Synchronizace produktu dle změna za 14 dní");
                    Console.WriteLine("5 = Synchronizace produktu dle ID");
                    Console.WriteLine("6 = Synchronizace produktu řádek od - do");
                    Console.WriteLine("7 = Synchronizace neprázdných kategoríí");
                    Console.WriteLine("8 = Výpis všech kategorií");
                    Console.WriteLine("9 = Výpis všech produktů");
                    Console.WriteLine("10 = Výpis všech jazyků");
                    Console.WriteLine("11 = Odstranění vazby na eshop");
                    Console.WriteLine("12 = Odstranění všech kategorií z eshopu");
                    Console.WriteLine("13 = Odstranění všech produktů");
                    Console.WriteLine("14 = Deaktivace neaktivních kategorií a produktů");
                    Console.WriteLine();
                    Console.WriteLine("50 = Odstranění logu");
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("Parametry notifikace:");
                    Console.WriteLine("0 (výchozí) - bez odesílání emailu");
                    Console.WriteLine("1 odeslat email jen v případě chyby");
                    Console.WriteLine("2 odeslat email vždy");
                    Console.WriteLine();
                    Console.WriteLine("Příklady:");
                    Console.WriteLine("Výpis url eshopu:                                        eshop_sync_sync.exe -r baseurl");
                    Console.WriteLine("Nastavení url eshopu:                                    eshop_sync_sync.exe -w baseurl http://mojedomena.cz");
                    Console.WriteLine();
                    Console.WriteLine("Test nastavení:                                          eshop_sync_sync.exe -t");
                    Console.WriteLine();
                    Console.WriteLine("synchronizovat kategorie bez oznámení emailu:            eshop_sync_sync.exe -s 1");
                    Console.WriteLine("synchronizovat kategorie s oznámení v případě chyb:      eshop_sync_sync.exe -s 1 1");
                    Console.WriteLine("synchronizovat produkty s oznámením                      eshop_sync_sync.exe -s 2 2");
                    Console.WriteLine();

                    break;
            }
        }
    }

    // vytvoření ftp složky
    public static void MakeFTPDir(string ftpAddress, string pathToCreate, string login, string password, byte[] fileContents, string ftpProxy = null)
    {
        FtpWebRequest reqFTP = null;
        System.IO.Stream ftpStream = null;

        string[] subDirs = pathToCreate.Split('/');

        string currentDir = string.Format("ftp://{0}", ftpAddress);
        SetLog("Vytvářím složku na FTP uložišti");
        foreach (string subDir in subDirs)
        {
            try
            {
                currentDir = currentDir + "/" + subDir;
                reqFTP = (FtpWebRequest)FtpWebRequest.Create(currentDir);
                reqFTP.Method = WebRequestMethods.Ftp.MakeDirectory;
                reqFTP.UseBinary = true;
                reqFTP.UsePassive = true;
                reqFTP.KeepAlive = true;
                reqFTP.Credentials = new NetworkCredential(login, password);
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                ftpStream = response.GetResponseStream();
                ftpStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                //SetChyba(string.Format("CHYBA: {0}", ex.Message.ToString()));
            }
        }
    }

    private static void AddAssociationProduct(AswLib.Tblvb tblwebcat, long category, long prdid, int pozice = 0)
    {
        SetLog("Vytvářím asociace k produktu", false);
        string webcats = GetWebsCats(tblwebcat, category);
        SetLog(" - " + webcats);
        string[] cats = webcats.Split(',');
        List<string> mycats = new List<string>();
        for (int a = 0; a < cats.Length; a++)
        {
            if (Convert.ToInt32(cats[a]) > 0)
            {
                if (!mycats.Contains(cats[a]))
                {
                    bool result = ExecuteSQL(string.Format("2|{0}|{1}|{2}", cats[a], prdid, pozice));
                    if (result)
                        SetLog(string.Format("{0} - OK", cats[a]));
                    else
                        SetChyba(string.Format("{0} - CHYBA", cats[a]));
                    mycats.Add(cats[a]);
                }
            }
        }
    }

    private static int AddCategory(int id, int parrent, string name, int position, bool active, bool root)
    {
        Bukimedia.PrestaSharp.Factories.CategoryFactory fct = new Bukimedia.PrestaSharp.Factories.CategoryFactory(Settings.Default.baseurl, Settings.Default.account, "");
        Bukimedia.PrestaSharp.Entities.category cat = new Bukimedia.PrestaSharp.Entities.category();
        cat.name.Add(new Bukimedia.PrestaSharp.Entities.AuxEntities.language(Settings.Default.default_lng, name));
        cat.link_rewrite.Add(new Bukimedia.PrestaSharp.Entities.AuxEntities.language(Settings.Default.default_lng, getRewriteFromName(name)));
        cat.position = position;
        cat.active = (active ? 1 : 0);
        cat.is_root_category = (root ? 1 : 0);
        cat.id_parent = parrent;
        try
        {
            cat = fct.Add(cat);
            if (cat.name[0].ToString() == name && cat.id_parent == parrent)
            {
                SetLog(string.Format("Kategorie byla úspěšně vytvořena id: {0}", cat.id));
                return Convert.ToInt32(cat.id);
            }
            else
            {
                return Settings.Default.default_catparent;
            }
        }
        catch (Exception ex)
        {
            SetChyba(string.Format("Vytvoření kategorie se nazdařilo !!!: {0}", ex.Message.ToString()));
        }

        return -1;
    }

    private static int  AddCombinationProduct(long prdid, long reference, int quantity, decimal price, bool isdefaulton, decimal sleva, List<Bukimedia.PrestaSharp.Entities.product_option_value> items)
    {
        Bukimedia.PrestaSharp.Factories.CombinationFactory fct = new Bukimedia.PrestaSharp.Factories.CombinationFactory(Settings.Default.baseurl, Settings.Default.account, "");

        Bukimedia.PrestaSharp.Entities.combination cmb = new Bukimedia.PrestaSharp.Entities.combination();
        cmb.id_product = prdid;
        cmb.wholesale_price = price;
        cmb.price = 0;
        cmb.ean13 = DateTime.Now.ToFileTimeUtc().ToString();
        cmb.quantity = quantity;
        cmb.minimal_quantity = 1;
        cmb.reference = reference.ToString();
        cmb.default_on = isdefaulton ? 1 : 0;

        cmb.available_date = DateTime.Now.ToString();
        int id_attribute = 0;
        for (int i = 0; i < items.Count; i++)
        {
            Bukimedia.PrestaSharp.Entities.AuxEntities.product_option_value pov = new Bukimedia.PrestaSharp.Entities.AuxEntities.product_option_value();
            pov.id = (long)items[i].id;

            cmb.associations.product_option_values.Add(pov);
        }

        bool chyba = false;
        try
        {
            cmb = fct.Add(cmb);
        }
        catch (Exception ex)
        {
            chyba = true;
        }
        if (cmb.id_product != prdid || cmb.reference != reference.ToString() || chyba)
        {
            string param = string.Format("4|{0}|{1}|{2}|{3}|{4}", prdid, reference, price.ToString().Replace(',', '.'), quantity, isdefaulton ? 1 : 0);
            bool exec = ExecuteSQL(param);
            //bool exec_shop = ExecuteSQL(string.Format("5 {0} {1} {2} {3} {4}", prdid, reference, price.ToString().Replace(',', '.'), quantity, isdefaulton ? 1 : 0));
            if (exec)
            {
                param = string.Format("22|{0}", reference);
                id_attribute = GetExecuteSQLId(param);
                if (id_attribute > 0)
                {
                    cmb = fct.Get(id_attribute);

                    for (int i = 0; i < items.Count; i++)
                    {
                        Bukimedia.PrestaSharp.Entities.AuxEntities.product_option_value pov = new Bukimedia.PrestaSharp.Entities.AuxEntities.product_option_value();
                        pov.id = (long)items[i].id;

                        cmb.associations.product_option_values.Add(pov);
                    }
                    try
                    {
                        fct.Update(cmb);
                        id_attribute = (int)cmb.id;
                    }
                    catch (Exception ex)
                    {
                    }


                }
                //Dictionary <string, string> dic = new Dictionary<string, string>();
                //dic.Add("reference", reference.ToString());
                //List<combination> lst = fct.GetByFilter(dic, "", "1");

                //if (lst != null)
                //{
                //    if (lst.Count > 0)

                //    {
                //        cmb = lst[0];

                //        for (int i = 0; i < items.Count; i++)
                //        {
                //            Bukimedia.PrestaSharp.Entities.AuxEntities.product_option_value pov = new Bukimedia.PrestaSharp.Entities.AuxEntities.product_option_value();
                //            pov.id = (long)items[i].id;

                //            cmb.associations.product_option_values.Add(pov);
                //        }
                //        try
                //        {
                //            fct.Update(cmb);
                //            id_attribute = (int) cmb.id;
                //        }
                //        catch (Exception ex)
                //        {
                //        }
                //    }
                //}
            }
        }
        if (id_attribute > 0)
        {
            SetLog(string.Format("Vytvořena nová kombinace id: {0}", id_attribute));
            stock_available stck = SetAmountProductCombination(prdid, (long)id_attribute, quantity);
            if (stck.id != null)
                SetLog("Zadáno množství");
            else
            {
                bool exec = ExecuteSQL(string.Format("6|{0}|{1}|{2}", prdid, id_attribute, quantity));
                if (exec)
                    SetLog("Zadáno množství");
            }

            if (sleva > 0)
            {
                SetLog("Sleva na produkt");
                AddProductSpecialPrice(prdid, (long)id_attribute, sleva);
            }
            return id_attribute;
        }
        SetLog("Kombinace nevytvořena !!!");
        return 0;
    }

    // aktualizace produktu
    private static void AddComplet(bool onlycat, bool onlyprod, bool novariant, bool setimg, int starti, int endi, bool all, int top, bool changes, int days = 0, long produkt = 0, bool discount = false, bool updcat = true)
    {
        //SetNullWebCategories();
        //DeletAllWebCategories();
        //DeletAllWebProducts();
        DateTime dtm = LastSync(days);
        SetLog(string.Format("Poslední synchronizace: {0}", dtm.ToString()));
        AswLib.Tblvb tblattr = GetAtributy();
        AswLib.Tblvb tbvar = GetVariantyProduktu();
        AswLib.Tblvb tblwebcat = GetKategorie();
        AswLib.Tblvb tblprd = GetProdukty(all, top, changes, dtm, produkt, discount);
        AswLib.Tblvb tblimg = GetImages(all, top, changes, dtm, produkt);
        AswLib.Tblvb tblvars = GetVariantyTable();
        AswLib.Tblvb tblnovar = GetProduktyNoVariant();
        //AswLib.Tblvb tblimgtyp = GetImageTypy();
        DeleteTempProduct();
        if (tblprd.Nacetldata & tblattr.Nacetldata & tbvar.Nacetldata)
        {
            if (endi > tblprd.Rows.Count)
                endi = tblprd.Rows.Count;
            if (endi == 0)
                SetLog("Nenalezen žádný záznam pro zpracování");

            for (int i = starti; i <= endi - 1; i++)
            {
                if (i % 20 == 0)
                {
                    SaveLog();
                }
                bool novy = false;
                SetLog("");
                SetLog("-------------------------------------------------------------------------------");
                SetLog(string.Format("Produkt {0}/{1} {2} id: {3} webid: {4}", i, tblprd.Rows.Count, tblprd.Rows[i]["name"].ToString(), tblprd.Rows[i]["ID"].ToString(), tblprd.Rows[i]["web_id"].ToString()));
                SetLog("-------------------------------------------------------------------------------");
                SetLog("");
                DataRow rw = tblprd.Rows[i];
                int cat = 0;
                long web_id = 0;
                int tax_group = 1;
                if (Convert.ToInt32(tblprd.Rows[i]["Vat"]) != Settings.Default.base_vat)
                    tax_group = 2;
                DataRow[] rvs = tblwebcat.Select(string.Format("ID={0}", rw["Category"]), "");
                DataRow[] varrvs = tbvar.Select(string.Format("Product={0} AND Total>0", rw["ID"]), "");
                int maxid = 0;
                if (rvs.Length > 0)
                {
                    cat = Convert.ToInt32(rvs[0]["web_id"]);
                }
                if (cat == 0)
                {
                    cat = GetWebCats(rvs[0], GetParentWebId(tblwebcat, Convert.ToInt32(rvs[0]["parent"])));
                    if (cat > 0)
                    {
                        if (onlycat)
                            continue;
                        if (Convert.ToInt32(rw["web_id"]) > 0)
                        {
                            web_id = Convert.ToInt32(rw["web_id"]);
                           

                            web_id = UpdateProduct(Convert.ToInt32(rw["ID"]), Convert.ToInt32(rw["web_id"]), cat, rw["name"].ToString(), rw["Specification"].ToString(), rw["ShortName"].ToString(), 0, 1, tax_group, (varrvs.Length == 0));
                            SetLog(string.Format("Reference na produkt nalezena v databázi: {0}", web_id));
                            rw["web_id"] = web_id;
                        }
                        else
                        {
                            web_id = GetProduct(Convert.ToInt32(rw["ID"]), Convert.ToInt32(rw["web_id"]), cat, rw["name"].ToString(), rw["Specification"].ToString(), rw["ShortName"].ToString(), 0, 1, tax_group, (varrvs.Length == 0));
                            if (web_id > 0)
                            {
                                rw["web_id"] = web_id;
                                novy = true;
                            }
                            else
                                continue;
                        }
                    }
                }
                else
                {
                    SetLog("Reference na kategorii nalezena v databázi");
                    if (updcat && cat > Settings.Default.default_catparent)
                        UpdateCategoryScript(cat, Convert.ToInt32(rvs[0]["webpar_id"]), rvs[0]["name"].ToString(), Convert.ToInt32(rvs[0]["position"]), (Convert.ToBoolean(rvs[0]["valid"]) & Convert.ToBoolean(rvs[0]["internet"])), false);

                    if (onlycat)
                        continue;
                    if (Convert.ToInt32(rw["web_id"]) > 0)
                    {
                        web_id = Convert.ToInt32(rw["web_id"]);
                       
                        web_id = UpdateProduct(Convert.ToInt32(rw["ID"]), Convert.ToInt32(rw["web_id"]), cat, rw["name"].ToString(), rw["Specification"].ToString(), rw["ShortName"].ToString(), 0, 1, tax_group, (varrvs.Length == 0));
                        SetLog(string.Format("Reference na produkt nalezena v databázi: {0}", web_id));
                        rw["web_id"] = web_id;
                    }
                    else
                    {
                        web_id = GetProduct(Convert.ToInt32(rw["ID"]), Convert.ToInt32(rw["web_id"]), cat, rw["name"].ToString(), rw["Specification"].ToString(), rw["ShortName"].ToString(), 0, 1, tax_group, (varrvs.Length == 0));
                        if (web_id > 0)
                            rw["web_id"] = web_id;
                        else
                            continue;
                    }
                }
                //viewcat.Dispose();

                //AddProductToCategories(web_id, cat);

                if (web_id > 0)
                {
                    DeletAssociationProuduct(web_id);
                    int pozice = Convert.ToInt32(rw["PlaceNo"]);
                    if (pozice < 0)
                        pozice = 0;

                    AddAssociationProduct(tblwebcat, Convert.ToInt64(rw["Category"]), web_id, pozice);

                    if (onlyprod)
                        continue;
                    //if (setimg)
                    //{
                    //    try
                    //    {
                    //        //DeleteProductImage(web_id);
                    //        SetProductImage(Convert.ToInt32(rw["ID"]), web_id, tblimg);
                    //    }

                    //    catch (Exception ex)
                    //    {
                    //        SetChyba(ex.Message.ToString());
                    //    }

                    //}

                    if (novariant)
                        continue;

                    DeleteProductCombination(web_id);
                    DeleteProductSpecialPrice(web_id);
                    bool default_on = false;

                    if (varrvs.Length > 0)
                    {
                        if (setimg)
                        {
                            try
                            {
                                //DeleteProductImage(web_id);

                                maxid = UpdateProductImage(Convert.ToInt32(rw["ID"]), web_id, tblimg, maxid, 0, 0, true, changes);
                            }
                            catch (Exception ex)
                            {
                                SetChyba(ex.Message.ToString());
                            }
                        }
                    }

                    for (int a = 0; a <= varrvs.Length - 1; a++)
                    {
                        DataRow rrw = varrvs[a];
                        DataRow rwvar = GetWebIdVarinat(tblvars, Convert.ToInt32(rrw["ID"]));
                        //int varid = 0;
                        //if (rwvar != null)
                        //    varid = Convert.ToInt32(rwvar["web_id"]);

                        //if (varid > 0)
                        //{
                        //    SetLog("Reference na variantu produktu nalezena v databázi");

                        //}
                        //else

                        //{
                        decimal cena = Convert.ToDecimal(rrw["pricenovat"]);

                        decimal sleva = 0;
                        if (Convert.ToDecimal(rrw["Discount"]) > 0)
                        {
                            sleva = Convert.ToDecimal(rrw["Discount"]);

                            sleva = sleva / 100;

                            //cena = cena - ((sleva / 100) * sleva);
                        }
                        //if (!default_on && (sleva > 0 || a==varrvs.Length-1))
                        //    default_on = true;
                        default_on = (a == 0);

                        DataView view = new DataView(tblattr, string.Format("Variant={0}", rrw["ID"]), "", DataViewRowState.OriginalRows);
                        List<Bukimedia.PrestaSharp.Entities.product_option_value> lst = GetAttribute(view.ToTable());
                        int mnozstvi = 0;
                        if (Convert.ToInt32(rrw["Total"]) > 0)
                            mnozstvi = Convert.ToInt32(rrw["Total"]);
                        long id_attribute = 0;
                       id_attribute = AddCombinationProduct(web_id, Convert.ToInt64(rrw["ID"]), mnozstvi, cena, default_on, sleva, lst);
                        if (rwvar != null && id_attribute>0)
                        {
                            rwvar["web_id"] =id_attribute;
                            if (setimg)
                            {
                                try
                                {
                                    //DeleteProductImage(web_id);
                                    maxid = UpdateProductImage(Convert.ToInt32(rw["ID"]), web_id, tblimg, maxid, Convert.ToInt64(rwvar["web_id"]), Convert.ToInt64(rrw["ID"]), true, changes);
                                }
                                catch (Exception ex)
                                {
                                    SetChyba(ex.Message.ToString());
                                }
                            }
                        }

                        //}
                    }

                    if (varrvs.Length == 0)
                    {
                        SetLog("Produkt nemá varianty");

                        if (setimg)
                        {
                            try
                            {
                                //DeleteProductImage(web_id);
                                maxid = UpdateProductImage(Convert.ToInt32(rw["ID"]), web_id, tblimg, maxid, 0, 0, true, changes);
                            }
                            catch (Exception ex)
                            {
                                SetChyba(ex.Message.ToString());
                            }
                        }

                        DeleteProductSpecialPrice(web_id);
                        DataRow[] rvdisc = tblnovar.Select(string.Format("ID={0}", rw["ID"]), "");
                        if (rvdisc.Length > 0)
                        {
                            AktualizujCenuProduktu(web_id, Convert.ToDecimal(rvdisc[0]["PriceNoVat"]));
                            if (Convert.ToDecimal(rvdisc[0]["Discount"]) > 0)
                            {
                                decimal sleva = Convert.ToDecimal(rvdisc[0]["Discount"]) / 100;
                                AddProductSpecialPrice(web_id, 0, sleva);
                            }
                        }
                        int mnozstvi = 0;
                        if (Convert.ToInt32(rw["Total"]) > 0)
                            mnozstvi = Convert.ToInt32(rw["Total"]);
                        stock_available stck = SetAmountProductCombination(web_id, 0, mnozstvi);
                        if (stck.id != null)
                            SetLog("Zadáno množství");
                        else
                        {
                            bool exec = ExecuteSQL(string.Format("6|{0}|{1}|{2}", web_id, 0, mnozstvi));
                            if (exec)
                                SetLog("Zadáno množství");
                        }
                    }
                }
            }
        }
        //SetWebParCats(tblwebcat);
        tblwebcat.AktualizujData();

        tblprd.AktualizujData();
        tbvar.AktualizujData();
        tblvars.AktualizujData();
        tblimg.AktualizujData();
    }

    private static Bukimedia.PrestaSharp.Entities.product_option AddNewOption(string vlastnost, bool iscolor, string group_type)
    {
        Bukimedia.PrestaSharp.Factories.ProductOptionFactory fct = new Bukimedia.PrestaSharp.Factories.ProductOptionFactory(Settings.Default.baseurl, Settings.Default.account, "");

        Bukimedia.PrestaSharp.Entities.product_option po = new Bukimedia.PrestaSharp.Entities.product_option();
        po.group_type = group_type;
        po.is_color_group = iscolor ? 1 : 0;
        po.name.Add(new Bukimedia.PrestaSharp.Entities.AuxEntities.language(Settings.Default.default_lng, vlastnost));
        po.public_name.Add(new Bukimedia.PrestaSharp.Entities.AuxEntities.language(Settings.Default.default_lng, vlastnost));
        po = fct.Add(po);
        if (po.id > 0)
        {
            SetLog(string.Format("Vytvořena nová vlastnost id: {0}", po.id));
            return po;
        }

        SetLog("Vlastnost nevytvořena!!!");
        return null;
    }

    private static Bukimedia.PrestaSharp.Entities.product_option_value AddNewOptionValue(long id_attribute_group, string hodnota, string color)
    {
        Bukimedia.PrestaSharp.Factories.ProductOptionValueFactory fct = new Bukimedia.PrestaSharp.Factories.ProductOptionValueFactory(Settings.Default.baseurl, Settings.Default.account, "");
        Bukimedia.PrestaSharp.Entities.product_option_value pov = new Bukimedia.PrestaSharp.Entities.product_option_value();
        pov.color = color;
        pov.id_attribute_group = id_attribute_group;
        pov.name.Add(new Bukimedia.PrestaSharp.Entities.AuxEntities.language(Settings.Default.default_lng, hodnota));

        pov = fct.Add(pov);
        if (pov.id > 0 && pov.name[0].Value == hodnota)
        {
            SetLog(string.Format("Hodnota vlastnosti vytvořena id: {0}", pov.id));
            return pov;
        }
        {
            string param = string.Format("13|{0}|{1}|{2}", id_attribute_group, Settings.Default.default_lng, getRequeststring(hodnota));
            int idatr = GetExecuteSQLId(param);
            if (idatr > 0)
            {
                pov = fct.Get(idatr);

                SetLog(string.Format("Hodnota vlastnosti vytvořena id: {0}", idatr));
                return pov;
            }
        }

        SetLog("Hodnota vlastnosti nebyla vytvořena!!!");
        return null;
    }

    // Vytvoření speciální ceny - slevy
    private static long AddProductSpecialPrice(long prdid, long prd_attr_id, decimal sleva)
    {
        Bukimedia.PrestaSharp.Factories.SpecificPriceFactory fct = new Bukimedia.PrestaSharp.Factories.SpecificPriceFactory(Settings.Default.baseurl, Settings.Default.account, "");
        Bukimedia.PrestaSharp.Entities.specific_price spc = new Bukimedia.PrestaSharp.Entities.specific_price();
        spc.id_product = prdid;
        spc.id_product_attribute = prd_attr_id;
        spc.price = -1;
        spc.reduction = sleva;
        spc.from_quantity = 1;
        spc.reduction_tax = 1;
        spc.id_specific_price_rule = 0;

        spc.id_shop = 0;
        spc.id_shop_group = 0;
        spc.id_currency = 0;
        spc.id_country = 0;
        spc.id_customer = 0;
        spc.id_group = 0;

        spc.id_cart = 0;
        spc.reduction_type = "percentage";

        spc = fct.Add(spc);
        if (spc.id > 0 && spc.id_product == prdid && spc.id_product_attribute == prd_attr_id)
        {
            SetLog(string.Format("Vytvořena sleva na produkt id: {0} kombinace id: {1}", prdid, prd_attr_id));
            return (long)spc.id;
        }
        else
        {
            string param = string.Format("7|{0}|{1}|{2}", prdid, prd_attr_id, sleva.ToString().Replace(',', '.'));
            bool exec = ExecuteSQL(param);
            if (exec)
                SetLog(string.Format("Vytvořena sleva na produkt id: {0} kombinace id: {1}", prdid, prd_attr_id));
            else
                SetLog(param);
        }

        return 0;
    }

    private static void AktivujProdukt(long web_id, bool active)
    {
        SetLog(string.Format("Aktivuji/Deaktivuji - web_id: {0}", web_id), false);
        bool result = ExecuteSQL(string.Format("20|{0}|{1}", web_id, active? 1:0));
        if (result)
            SetLog(" - OK");
        else
            SetChyba(" - CHYBA");
    }
    private static void AktivujKategorii(long web_id, bool active)
    {
        SetLog(string.Format("Aktivuji/Deaktivuji - web_id: {0}", web_id), false);
        bool result = ExecuteSQL(string.Format("21|{0}|{1}", web_id, active ? 1 : 0));
        if (result)
            SetLog(" - OK");
        else
            SetChyba(" - CHYBA");
    }

    // aktualizace ceny produktu
    private static void AktualizujCenuProduktu(long web_id, decimal v)
    {
        SetLog(string.Format("Aktualizuji cenu - web_id: {0}, cena: {1}", web_id, v.ToString("N2")), false);
        bool result = ExecuteSQL(string.Format("3|{1}|{0}", web_id, v.ToString().Replace(',', '.')));
        if (result)
            SetLog(" - OK");
        else
            SetChyba(" - CHYBA");
    }

    // Aktualizace kategorí
    private static void AnalyzeCategories(int oznameni, bool all = true, int top = 0, bool notzero = false)
    {
        _log = "";
        _chyb = 0;
        DateTime dtm = DateTime.Now;
        StartApp("analýza kategorií");

        AswLib.Tblvb dt = GetKategorie(all, top, notzero);

        if (dt != null)
        {
            SetWebCats(dt, true);
        }

        dt.Dispose();

        double second = (DateTime.Now - dtm).TotalSeconds;
        dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

        dtm = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0).AddSeconds(second);

        EndApp(true, oznameni);
    }

    // Aktualizace kategorií
    private static void AnalyzeCategories(int oznameni)
    {
        _log = "";
        _chyb = 0;
        DateTime dtm = DateTime.Now;
        StartApp("analýza kategorií");

        AswLib.Tblvb dt = GetKategorie();

        if (dt != null)
        {
            SetWebCats(dt);
        }

        dt.Dispose();

        double second = (DateTime.Now - dtm).TotalSeconds;
        dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

        dtm = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0).AddSeconds(second);

        EndApp(true, oznameni);
    }

    private static void AnalyzeProducts(int oznameni)
    {
        StartApp("Kompletní analýza produktů");
        AddComplet(false, false, false, false, 0, 10000, true, 0, false, 0, 0, false, false);
        EndApp(true, oznameni);
    }

    private static void ClearLogs()
    {
        CheckLogDir();
        StartApp("Mazání logu");
        string dir = string.Format("{0}\\Log", System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));
        string[] soubory = System.IO.Directory.GetFiles(dir);

        SetLog(string.Format("Počet nalezených souborů: {0}", soubory.Length));
        for (int i = 0; i <= soubory.Length - 1; i++)
        {
            try
            {
                SetLog(string.Format("Odstraňuji [{0}]", soubory[i]), false);
                System.IO.File.Delete(soubory[i]);
                SetLog(" - OK");
            }
            catch (Exception ex)
            {
                SetChyba(string.Format(" - chyba: {0}", ex.Message.ToString()));
            }
        }
        EndApp(false);
    }

    // kopírování obrázů na FTP eshopu
    private static void CopyImagesToFTP(string dir)
    {
        string[] soubory = System.IO.Directory.GetFiles(dir);

        SetLog(string.Format("Počet nalezených souborů: {0}", soubory.Length));
        for (int i = 0; i <= soubory.Length - 1; i++)
        {
            string imgname = System.IO.Path.GetFileNameWithoutExtension(soubory[i]).Substring(0, 4);
            string path = GetPathImg("", Convert.ToInt32(imgname));
            path = path.Replace("\\", "/");
            SaveFileToFTP(soubory[i], string.Format("www/img/p/{0}", path));
            //System.IO.File.Copy("C:\\microdata\\img\\filetype", path + "\\filetype");
            //System.IO.File.Copy("C:\\microdata\\img\\index.php", path + "\\index.php");
            SaveFileToFTP("C:\\microdata\\img\\filetype", string.Format("www/img/p/{0}", path));
            SaveFileToFTP("C:\\microdata\\img\\index.php", string.Format("www/img/p/{0}", path));
            SetLog(string.Format("{0}/{1} {2}", i + 1, soubory.Length, imgname));
        }
    }

    // Odstranění všech kategorií
    private static void DeletAllWebCategories()
    {
        StartApp("Odstranění všech kategorií");
        Bukimedia.PrestaSharp.Factories.CategoryFactory fct = new Bukimedia.PrestaSharp.Factories.CategoryFactory(Settings.Default.baseurl, Settings.Default.account, "");

        SetLog("Načítám kategorie", false);
        List<long> lst = fct.GetIds();
        if (lst != null)
        {
            SetLog(string.Format(" CELKEM: {0}", lst.Count));

            for (int i = lst.Count - 1; i >= 0; i += -1)
            {
                Bukimedia.PrestaSharp.Entities.category cat = fct.Get(lst[i]);
                if (cat.id > 2 & cat.is_root_category == 0)
                {
                    try
                    {
                        SetLog(string.Format("Odstraňuji {0}-{1}", cat.id, cat.name[0].Value), false);
                        fct.Delete(cat);
                        SetLog(" - OK");
                    }
                    catch (Exception ex)
                    {
                        SetChyba(string.Format(" - CHYBA: {0}", ex.Message.ToString()));
                    }
                }
            }
        }

        EndApp();
    }

    private static void DeletAssociationProuduct(long web_id)
    {
        SetLog("Odstraňuji asociace produktu", false);
        bool result = ExecuteSQL(string.Format("1|{0}", web_id));
        if (result)
            SetLog(" - OK");
        else
            SetChyba(" - CHYBA");
    }

    private static void DeleteAllWebProducts()
    {
        StartApp("Odstranění produktů");
        Bukimedia.PrestaSharp.Factories.ProductFactory fct = new Bukimedia.PrestaSharp.Factories.ProductFactory(Settings.Default.baseurl, Settings.Default.account, "");

        SetLog("Načítám produkty", false);
        List<long> lst = fct.GetIds();
        if (lst != null)
        {
            SetLog(string.Format(" CELKEM: {0}", lst.Count));

            for (int i = 0; i <= lst.Count - 1; i++)
            {
                Bukimedia.PrestaSharp.Entities.product prd = fct.Get(lst[i]);
                if (prd != null)
                {
                    if (prd.id > 0)
                    {
                        try
                        {
                            SetLog(string.Format("Odstraňuji {0}-{1}", prd.id, prd.name[0].Value), false);
                            DeleteProductImage((long)prd.id);
                            DeleteProductCombination((long)prd.id);
                            DeleteProductSpecialPrice((long)prd.id);
                            fct.Delete(prd);
                            SetLog(" - OK");
                        }
                        catch (Exception ex)
                        {
                            SetChyba(string.Format(" - CHYBA: {0}", ex.Message.ToString()));
                        }
                    }
                }
            }
        }

        EndApp();
    }  // odstranění obrázku produktu PRESTASHARP

    // Přidání obrázku k produktu PRESTASHARP

    private static bool DeleteDir(string dir)

    {
        string[] soubory = System.IO.Directory.GetFiles(dir);

        SetLog(string.Format("Počet nalezených souborů: {0}", soubory.Length));
        for (int i = 0; i <= soubory.Length - 1; i++)
        {
            System.IO.File.Delete(soubory[i]);
        }

        return true;
    }

    private static void DeleteKategorieProductMySQL()
    {
        SetLog("Odstraňuji vazby", false);
        AswLib.Pripojeni.SQL prip = new AswLib.Pripojeni.SQL(Settings.Default.cnn_priority + 1);
        prip.Cmd.CommandText = "delete from ps_category_product";
        prip.Cmd.CommandType = CommandType.Text;
        prip.Exec();
        if (prip.SQLAno)
            SetLog(" - OK");
        else
            SetChyba(" - chyba: " + prip.Chyba);
    }

    private static void DeleteProductCombination(long prdid)
    {
        Bukimedia.PrestaSharp.Factories.CombinationFactory fct = new Bukimedia.PrestaSharp.Factories.CombinationFactory(Settings.Default.baseurl, Settings.Default.account, "");

        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("id_product", prdid.ToString());

        List<Bukimedia.PrestaSharp.Entities.combination> lst = fct.GetByFilter(dic, "", "");
        if (lst.Count > 0)
        {
            SetLog(string.Format("Nalezeno {0} kombinací", lst.Count));

            for (int i = 0; i < lst.Count; i++)
            {
                try
                {
                    if (lst[i].id_product == prdid)
                    {
                        SetLog(string.Format("Odstraňuji kombinaci id: {0}", lst[i].id), false);
                        fct.Delete(lst[i]);
                        SetLog(" - OK");
                    }
                }
                catch (Exception ex)
                {
                    bool exec = ExecuteSQL(string.Format("5|{0}", prdid));
                    if (exec)
                        SetLog("Kombinace odstraněna");
                    else
                        SetChyba(string.Format(" - CHYBA: {0}", ex.Message.ToString()));
                }
            }
        }
        else
        {
            bool exec = ExecuteSQL(string.Format("5|{0}", prdid));
            if (exec)
                SetLog("Kombinace odstraněna");
        }
    }

    private static void DeleteProductImage(long prdid)
    {
        SetLog("Odstraňuji obrázky");
        Bukimedia.PrestaSharp.Factories.ImageFactory fct = new Bukimedia.PrestaSharp.Factories.ImageFactory(Settings.Default.baseurl, Settings.Default.account, "");
        List<Bukimedia.PrestaSharp.Entities.FilterEntities.declination> lst = null;
        try
        {
            lst = fct.GetProductImages(prdid);
        }
        catch (Exception ex)
        {
        }

        if (lst != null)

        {
            if (lst.Count > 0)
            {
                SetLog(string.Format("Nalezeno {0} obrázků", lst.Count));

                for (int i = 0; i < lst.Count; i++)
                {
                    try
                    {
                        SetLog(string.Format("Odstraňuji obrázek id: {0}", lst[i].id), false);
                        fct.DeleteProductImage(prdid, lst[i].id);
                        SetLog(" - OK");
                    }
                    catch (Exception ex)
                    {
                        SetChyba(string.Format(" - CHYBA: {0}", ex.Message.ToString()));
                    }
                }
            }
        }
    }

    // Odstranění speciální ceny - slevy
    private static void DeleteProductSpecialPrice(long prdid)
    {
        Bukimedia.PrestaSharp.Factories.SpecificPriceFactory fct = new Bukimedia.PrestaSharp.Factories.SpecificPriceFactory(Settings.Default.baseurl, Settings.Default.account, "");

        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("id_product", prdid.ToString());
        List<Bukimedia.PrestaSharp.Entities.specific_price> lst = fct.GetByFilter(dic, "", "");

        if (lst != null)
        {
            if (lst.Count > 0)
            {
                SetLog(string.Format("Nalezeno {0} speciálních cen", lst.Count));

                for (int i = 0; i < lst.Count; i++)
                {
                    try
                    {
                        SetLog(string.Format("Odstraňuji speciální cenu id: {0}", lst[i].id), false);
                        fct.Delete(lst[i]);
                        SetLog(" - OK");
                    }
                    catch (Exception ex)
                    {
                        SetChyba(string.Format(" - CHYBA: {0}", ex.Message.ToString()));
                    }
                }
            }
        }
    }

    private static void DeleteTempProduct()
    {
        Tblvb dt = new Tblvb();
        dt.NactiData(Settings.Default.cnn_priority, "Select max(web_id) from products");
        if (dt.Nacetldata)
        {
            if (dt.Rows.Count > 0)
            {
                int prdidfrom = Convert.ToInt32(dt.Rows[0][0]);
                //Bukimedia.PrestaSharp.Factories.ProductFactory fct = new Bukimedia.PrestaSharp.Factories.ProductFactory(Settings.Default.baseurl, Settings.Default.account, "");
                //List<long> lst = fct.GetIds();

                //for (int i = 0; i < lst.Count; i++)
                //{
                //    if (lst[i] > prdidfrom)
                //    {
                //        fct.Delete(lst[i]);

                //    }

                //}
                bool upd = ExecuteSQL(string.Format("9|{0}", prdidfrom));
                if (upd)
                    SetLog("OK");
            }
        }
    }

    private static void EndApp(bool save = true, int oznameni = 0)
    {
        double second = (DateTime.Now - _dtm).TotalSeconds;
        _dtm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
        _dtm = new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0).AddSeconds(second);
        string file = "";
        string result = string.Format("Aplikace [{0}] je dokončena počet chyb: {1}, {2} celkový čas: {3}", _app, _chyb, DateTime.Now.ToString(), _dtm.ToString("mm:ss"));
        SetLog(result, true);
        if (save)
        {
            file = SaveLog();
        }

        if ((oznameni == 1 & _chyb > 0) | oznameni == 2)
        {
            AswLib.Mail.MailObj ml = new AswLib.Mail.MailObj();
            ml.OznamovaciMail(Settings.Default.senderemail, Settings.Default.emaillist, string.Format("{0} - {1}", _app, (_chyb > 0 ? "CHYBA!!!" : "OK")), result, file, Settings.Default.smtp_srv, Settings.Default.smtp_user, Settings.Default.smtp_pwd, Settings.Default.smtp_port);
        }
    }

    // Pokusná procedura
    private static void Example()

    {
        //GetUpdateProductsPriceNoVariantScript();
        //UpdatePriceMysql();
        UpdateAllProductImage(false, true);
        //GetUpdateImageScript();
        //GetUpdateCategoriesMySqlScript();
        //StartApp("pokus");
        //UpdateCategoryScript(0, 2, "ahoj", 0, true, false);
        //EndApp();
        //CopyFile("C:\\microdata\\img\\7\\2\\1\\7\\7217.jpg", "www/honza/audy/7/2/1/7/7217.jpg");
        //SaveFileToFTP("C:\\microdata\\img\\7\\2\\1\\7\\7217.jpg", "www/honza/audy/7/2/1/7");
        //CopyImagesToFTP("C:\\microdata\\p");

        //Bukimedia.PrestaSharp.Entities.product_option po = GetOption("Size", false, "select");
        //if (po != null)
        //    SetLog(string.Format("{0}", po.id));

        //Bukimedia.PrestaSharp.Entities.product_option_value pov = GetOptionValue(1,"S","");
        //if (pov != null)
        //    SetLog(string.Format("{0}", pov.id));

        //AddComplet();
        //SetAssociationProduct();
        //UpdateAllZeroAmountProduct();
        //UpdatePriceProductZeroVariantMysql();
        //GetUpdateCategoriesScript();
        //GetUpdateProductsScript();
        //GetUpdateProductsPriceNoVariantScript();
        //GetUpdateProductsAmountScript();
        //GetUpdateProductsVariantAmountScript();
        //GetUpdateProductVariantScript();
        //GetiNetUpdateProductsVariantAmountScript();
        //bool result = ExecuteSQL("0 20000 91 0");
        //if (result)
        //    SetLog("OK");
        //else
        //    SetLog("CHYBA");
        //LogInsertTemplateSQL("ps_product");
        //LogInsertTemplateSQL("ps_product_lang");
        //LogInsertTemplateSQL("ps_product_shop");
        //string mystr = "ěščřžýáíéťďňůú,ŽŠČŘŇĎŤĚÉÝÚ";
        //string str = getRequeststring(mystr);
        //str = string.Format("12|{0}", str);
        ////SetLog(str);

        //System.Text.Encoding utf8 = System.Text.Encoding.UTF8;
        //byte[] utf8Bytes = utf8.GetBytes(mystr);
        //SetLog(string.Join(";", utf8Bytes));
        //char[] utf8Chars = new char[utf8.GetCharCount(utf8Bytes, 0,utf8Bytes.Length)];
        //utf8.GetChars(utf8Bytes, 0, utf8Bytes.Length, utf8Chars, 0);
        //string utf8String = new string(utf8Chars);
        //SetLog(utf8String);
        //str = string.Format("12|{0}", string.Join(";", utf8Bytes));
        //string s = "ěščřžýáíéťďňůú,ŽŠČŘŇĎŤĚÉÝÚ";
        //int i = GetIntFromString(s);
        //SetLog(i.ToString());

        //Console.WriteLine(str);

        //AddComplet(true, false, false, true, 0, 10000, false, 0, true);
        //Settings.Default.last_sync = DateTime.Now;
        //Bukimedia.PrestaSharp.Factories.ProductFactory fct = new Bukimedia.PrestaSharp.Factories.ProductFactory(Settings.Default.baseurl, Settings.Default.account, "");
        //product prd = fct.Get(3580);
        //DeleteTempProduct();
        //Bukimedia.PrestaSharp.Factories.ImageFactory fct = new Bukimedia.PrestaSharp.Factories.ImageFactory(Settings.Default.baseurl, Settings.Default.account, "");
        //List<Bukimedia.PrestaSharp.Entities.FilterEntities.declination> lst = fct.GetProductImages(3530);
        //if (lst != null)
        //{
        //}
        //SetProductImage(0, 3530, null);
    }

    private static bool ExecuteSQL(string param)
    {
        string url = Settings.Default.baseurl.Replace("/api", "/products.php");
        Bukimedia.PrestaSharp.Factories.inetfactory inet = new Bukimedia.PrestaSharp.Factories.inetfactory(url, "", "");
        return inet.GetResult(param);
    }

    private static AswLib.Tblvb GetAtributy(bool all = true, int top = 0)
    {
        SetLog("Načítám atributy", false);
        AswLib.Tblvb dt = new AswLib.Tblvb();
        if (all)
            dt.NactiData(Settings.Default.cnn_priority, "select * from ProductVariantAttributes inner join ProductAttribute on ProductVariantAttributes.Attribute = ProductAttribute.id inner join ProductAttributeGroup on ProductAttribute.[Group] = productattributegroup.id", "ProductVariantAttributes");
        else if (top > 0)
            dt.NactiData(Settings.Default.cnn_priority, string.Format("select TOP {0} * from ProductVariantAttributes inner join ProductAttribute on ProductVariantAttributes.Attribute = ProductAttribute.id inner join ProductAttributeGroup on ProductAttribute.[Group] = productattributegroup.id", top), "ProductVariantAttributes");

        if (dt.Nacetldata)
        {
            SetLog(" - OK");
        }
        else
        {
            SetChyba("Chyba při načítání tabulky atributy" + dt.Chyba);
        }

        return dt;
    }

    private static List<product_option_value> GetAttribute(DataTable tblvars)
    {
        List<product_option_value> mylst = new List<product_option_value>();

        for (int c = 0; c <= tblvars.Rows.Count - 1; c++)
        {
            DataRow mrv = tblvars.Rows[c];
            bool iscolor = Convert.ToBoolean(mrv["IsColor"]);
            string group_type = iscolor ? "color" : "select";
            string color = "";
            if (mrv["color"] != System.DBNull.Value)
                color = mrv["color"].ToString();

            Bukimedia.PrestaSharp.Entities.product_option po = GetOption(mrv["Name"].ToString(), iscolor, group_type);

            if (po == null)

            {
                po = AddNewOption(mrv["Name"].ToString(), iscolor, group_type);
            }
            if (po != null)
            {
                Bukimedia.PrestaSharp.Entities.product_option_value pov = GetOptionValue((long)po.id, mrv["Value"].ToString(), color);

                if (pov != null)
                {
                    mylst.Add(pov);
                }
            }
        }
        return mylst;
    }

    private static string GetCategoryTree(Bukimedia.PrestaSharp.Factories.CategoryFactory fct, long id)
    {
        Bukimedia.PrestaSharp.Entities.category cat = fct.Get(id);
        if (cat != null)
        {
            if (cat.id_parent > 0)
            {
                return string.Format("{0}/{1}", GetCategoryTree(fct, (long)cat.id_parent), cat.name[0].Value);
            }
            else
                return cat.name[0].Value;
        }
        return "";
    }

    // spušetění scriptu PHP
    // funkce vracející id po spuštění scriptu PHP
    private static int GetExecuteSQLId(string param)
    {
        string url = Settings.Default.baseurl.Replace("/api", "/products.php");
        Bukimedia.PrestaSharp.Factories.inetfactory inet = new Bukimedia.PrestaSharp.Factories.inetfactory(url, "", "");
        string res = inet.GetResultValue(param);
        if (IsNumeric(res))
            return Convert.ToInt32(res);

        return 0;
    }

    private static System.Drawing.Image GetImageFromBytes(byte[] byteArrayIn)
    {
        try
        {
            System.IO.MemoryStream ms = new System.IO.MemoryStream(byteArrayIn, 0, byteArrayIn.Length);
            ms.Write(byteArrayIn, 0, byteArrayIn.Length);
            //Exception occurs here
            return System.Drawing.Image.FromStream(ms, true);
        }
        catch
        {
        }
        return null;
    }

    private static AswLib.Tblvb GetImages(bool all = true, int top = 0, bool changes = false, DateTime dtm = new DateTime(), long produkt = 0)
    {
        SetLog("Načítám obrázky", false);
        AswLib.Tblvb dt = new AswLib.Tblvb();
        if (all)
            dt.NactiData(Settings.Default.cnn_priority, "select Id, Product, ISNULL(Variant,0) as Variant, Image, Position, web_id from ProductImages where valid=1 order by product, Variant", "ProductImages");
        //dt.NactiData(Settings.Default.cnn_priority, "select product, [Image], max(variant) as Variant from ProductImages where valid = 1 group by product, [Image] order by product, Variant", "ProductImages");
        else if (top > 0)
            dt.NactiData(Settings.Default.cnn_priority, string.Format("select TOP {0}  select Id, Product, ISNULL(Variant,0) as Variant, Image, Position, web_id from ProductImages where valid=1 order by product, Variant", top), "ProductImages");
        else if (produkt > 0)
            dt.NactiData(Settings.Default.cnn_priority, string.Format("select Id, Product, ISNULL(Variant,0) as Variant, Image, Position, web_id from ProductImages where valid = 1 and product={0} order by product, Variant", produkt), "ProductImages");
        else if (changes)
        {
            string sql = string.Format("select Id, Product, ISNULL(Variant,0) as Variant, Image, Position, web_id from ProductImages where valid = 1 and product in (select id from products where Valid=1 and Setting in (select id from ProductSettings where Internet = 1) and (id in (select itemid from changes where TableName='Products' and Ts>=dateadd(s,{0},getdate())) or web_id=0 or id in (select product from StoreProducts where lastchange >=dateadd(s,{0},getdate())))) order by product, Variant", (int)(dtm - DateTime.Now).TotalSeconds);
            dt.NactiData(Settings.Default.cnn_priority, sql, "ProductImages");
            //dt.NactiData(Settings.Default.cnn_priority, "select * from ProductImages where valid = 1 order by product, Variant", "ProductImages");
        }
        if (dt.Nacetldata)
        {
            SetLog(" - OK");
        }
        else
        {
            SetChyba("Chyba při načítání tabulky obrázků" + dt.Chyba);
        }

        return dt;
    }

    private static AswLib.Tblvb GetImageTypy()
    {
        SetLog("Načítám typy obrázků", false);
        AswLib.Tblvb dt = new AswLib.Tblvb();

        dt.NactiData(Settings.Default.cnn_priority + 1, "SELECT * from ps_image_type where products=1", "imgtyp");

        if (dt.Nacetldata)
        {
            SetLog(" - OK");
        }
        else
        {
            SetChyba("Chyba při načítání tabulky typů obrázků" + dt.Chyba);
        }

        return dt;
    }

    private static void GetiNetUpdateProductsVariantAmountScript()
    {
        AswLib.Tblvb dt = GetVariantyProduktu();

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (Convert.ToInt32(dt.Rows[i]["web_id"]) > 0 && Convert.ToInt32(dt.Rows[i]["prdid"]) > 0)
            {
                int mnozstvi = 0;
                if (Convert.ToInt32(dt.Rows[i]["Total"]) > 0)
                    mnozstvi = Convert.ToInt32(dt.Rows[i]["Total"]);
                bool result = ExecuteSQL(string.Format("{0}|{1}|{2}|{3}", 0, mnozstvi, dt.Rows[i]["web_id"], dt.Rows[i]["prdid"]));
                if (result)
                    SetLog(string.Format("{0} - OK", dt.Rows[i]["web_id"]));
            }
        }
        EndApp();
    }

    private static AswLib.Tblvb GetKategorie(bool all = true, int top = 0, bool notzero = false, bool active = true)
    {
        SetLog("Načítám kategorie", false);
        AswLib.Tblvb dt = new AswLib.Tblvb();
        if (all)
            if (active)
            {
                dt.NactiData(Settings.Default.cnn_priority, "Select  *, convert(int,0) as level from Categories where id>1 order by parent, position", "kategorie");
            }
            else {

                dt.NactiData(Settings.Default.cnn_priority, string.Format("select * from Categories where id in (select Category from products where (Setting in (select id from ProductSettings where Internet=0) and web_id >0) or Internet=0) and web_id >{0}",Settings.Default.default_catparent), "kategorie");
            }
       
        else if (top > 0)
            dt.NactiData(Settings.Default.cnn_priority, string.Format("Select TOP {0} *, convert(int,0) as level from Categories where id>1 order by parent, position", top), "kategorie");
        else if (notzero)
            dt.NactiData(Settings.Default.cnn_priority, "Select  *, convert(int,0) as level from Categories where id>1 and web_id >0 and webpar_id>0 order by parent, position", "kategorie");
        if (dt.Nacetldata)
        {
            SetLog(" - OK");
        }
        else
        {
            SetChyba("Chyba při načítání tabulky kategorií" + dt.Chyba);
        }

        if (active)
        {
            SetLevel(dt);
            dt.AcceptChanges();
            dt.Select("", "level ASC");

        }

        
        return dt;
    }

    // odstranění vazeb produkt kategorie MySQL
    // funkce vracející tabulku vazeb produkt, kategorie
    private static AswLib.Tblvb GetKategorieProduct()
    {
        DeleteKategorieProductMySQL();
        SetLog("Načítám vazby kategorie, produkt", false);
        AswLib.Tblvb dt = new AswLib.Tblvb();

        dt.NactiData(Settings.Default.cnn_priority + 1, "Select * from  ps_category_product", "category_product");
        //and internet = 1 and valid = 1

        if (dt.Nacetldata)
        {
            SetLog(" - OK");
        }
        else
        {
            SetChyba("Chyba při načítání tabulky kategorií" + dt.Chyba);
        }

        return dt;
    }

    private static int GetLevel(AswLib.Tblvb dt, int parent, int counter)
    {
        DataView view = new DataView(dt, string.Format("ID={0}", parent), "ID", DataViewRowState.OriginalRows);
        int myparent = 0;

        if (view.ToTable().Rows.Count > 0)
        {
            myparent = Convert.ToInt32(view.ToTable().Rows[0]["Parent"]);
        }
        view.Dispose();
        if (myparent > 0)
        {
            return GetLevel(dt, myparent, counter + 1);
        }

        return counter;
    }

    private static Bukimedia.PrestaSharp.Entities.product_option GetOption(string vlastnost, bool iscolor, string group_type)
    {
        Bukimedia.PrestaSharp.Factories.ProductOptionFactory fct = new Bukimedia.PrestaSharp.Factories.ProductOptionFactory(Settings.Default.baseurl, Settings.Default.account, "");
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("name", vlastnost);
        dic.Add("group_type", group_type);
        dic.Add("is_color_group", (iscolor ? 1 : 0).ToString());
        List<Bukimedia.PrestaSharp.Entities.product_option> lst = fct.GetByFilter(dic, "name_ASC", "1");
        if (lst != null)
        {
            if (lst.Count > 0)
            {
                SetLog(string.Format("Nalezena vlastnost id: {0}", lst[0].id));
                return lst[0];
            }
        }
        SetLog("Vlastnost nenalezena!!!");
        return null;
    }

    private static Bukimedia.PrestaSharp.Entities.product_option_value GetOptionValue(long id_attribute_group, string hodnota, string color)
    {
        Bukimedia.PrestaSharp.Factories.ProductOptionValueFactory fct = new Bukimedia.PrestaSharp.Factories.ProductOptionValueFactory(Settings.Default.baseurl, Settings.Default.account, "");
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("name", hodnota);
        dic.Add("id_attribute_group", id_attribute_group.ToString());
        //dic.Add("color", color);
        List<Bukimedia.PrestaSharp.Entities.product_option_value> lst = fct.GetByFilter(dic, "name_ASC", "1");

        if (lst != null)
        {
            if (lst.Count > 0)
            {
                SetLog(string.Format("Hodnota vlastnosti nalezena id: {0}", lst[0].id));
                return lst[0];
            }
            else
            {
                lst.Add(AddNewOptionValue(id_attribute_group, hodnota, color));
                return lst[lst.Count - 1];
            }
        }
        SetLog("Hodnota vlastnosti nenalezena !!!");
        return null;
    }

    private static AswLib.Tblvb GetPackagesTable(bool all = true, int top = 0)
    {
        SetLog("Načítám Packages", false);
        AswLib.Tblvb dt = new AswLib.Tblvb();
        if (all)
            dt.NactiData(Settings.Default.cnn_priority, "select * from Packages order by product", "varianty");
        else if (top > 0)
            dt.NactiData(Settings.Default.cnn_priority, string.Format("select TOP {0} * from Packages order by product", top), "varianty");

        if (dt.Nacetldata)
        {
            SetLog(" - OK");
        }
        else
        {
            SetChyba("Chyba při načítání tabulky packages" + dt.Chyba);
        }

        return dt;
    }

    // funkce vracející id nadřazené kategorie
    private static int GetParentWebId(AswLib.Tblvb dt, int parent)
    {
        DataRow[] rows = dt.Select(string.Format("Id={0}", parent), "");
        if (rows.Length > 0)
        {
            if (Convert.ToInt32(rows[0]["web_id"]) > 0)
            {
                SetLog(string.Format("Parent id: {0}", rows[0]["web_id"]));
                return Convert.ToInt32(rows[0]["web_id"]);
            }
            else
            {
                int mparent = GetParentWebId(dt, Convert.ToInt32(rows[0]["parent"]));
                return GetWebCats(rows[0], mparent);
            }
        }
        else
        {
            SetLog(string.Format("Parent id: {0}", Settings.Default.default_catparent));
            return Settings.Default.default_catparent;
        }
    }

    // funkce vracející adresář obrázku
    private static string GetPathImg(string root, int idimage)
    {
        string idstring = idimage.ToString();
        for (int i = 0; i <= idstring.Length - 1; i++)
        {
            root += string.Format("\\{0}", idstring.Substring(i, 1));
        }
        return root;
    }

    private static int GetProduct(int id, int web_id, int id_category, string nazev, string popis, string shortpopis, decimal cena, decimal mnozstvi, int tax_group, bool novariant)
    {
        Bukimedia.PrestaSharp.Factories.ProductFactory fct = new Bukimedia.PrestaSharp.Factories.ProductFactory(Settings.Default.baseurl, Settings.Default.account, "");
        product myprd = new product();

        web_id = 0;
        myprd = new Bukimedia.PrestaSharp.Entities.product();
        myprd.name.Add(new Bukimedia.PrestaSharp.Entities.AuxEntities.language(Settings.Default.default_lng, nazev));
        myprd.description.Add(new Bukimedia.PrestaSharp.Entities.AuxEntities.language(Settings.Default.default_lng, popis));
        myprd.description_short.Add(new Bukimedia.PrestaSharp.Entities.AuxEntities.language(Settings.Default.default_lng, shortpopis));
        myprd.link_rewrite.Add(new Bukimedia.PrestaSharp.Entities.AuxEntities.language(Settings.Default.default_lng, getRewriteFromName(nazev)));
        myprd.available_for_order = 1;
        myprd.show_price = 1;
        myprd.id_tax_rules_group = tax_group;
        myprd.id_shop_default = 1;
        myprd.New = 1;

        myprd.on_sale = 1;
        myprd.position_in_category = 0;
        //myprd.associations.categories.Add(new Bukimedia.PrestaSharp.Entities.AuxEntities.category((long)id_category));

        //myprd.id_category_default = id_category;

        try
        {
            SetLog("V eshopu nenalezen!!! Vytvářím nový produkt.", false);
            myprd = fct.Add(myprd);
            if (myprd.id > 0 && myprd.reference == id.ToString())
            {
                //Bukimedia.PrestaSharp.Entities.AuxEntities.category ctg = new Bukimedia.PrestaSharp.Entities.AuxEntities.category();
                //ctg.id = id_category;
                //myprd.associations.categories.Add(ctg);
                //fct.Update(myprd);

                SetLog(string.Format(" - OK id: {0}", myprd.id));
                return Convert.ToInt32(myprd.id);
            }
            else
            {
                string param = string.Format("8|{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}", 0, id, getRequeststring(nazev), getRequeststring("-"), getRequeststring(shortpopis), getRequeststring(getRewriteFromName(nazev)), Settings.Default.default_lng, tax_group);
                SetLog(param);
                int prdid = GetExecuteSQLId(param);
                //bool exec_shop = ExecuteSQL(string.Format("5 {0} {1} {2} {3} {4}", prdid, reference, price.ToString().Replace(',', '.'), quantity, isdefaulton ? 1 : 0));
                if (prdid > 0)
                {
                    param = string.Format("14|{0}|{1}", prdid, getRequeststring(popis));
                    //bool upd = UpdateProduct(id, prdid, id_category, nazev, popis, shortpopis, cena, mnozstvi, tax_group, novariant);

                    bool upd = ExecuteSQL(param);
                    if (upd)
                    {
                        SetLog(string.Format(" - OK id: {0}", prdid));
                        return prdid;
                    }
                    else
                    {
                        SetLog(param);
                        //SetChyba(" - CHYBA - NEZNÁMÁ CHYBA");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            SetChyba(string.Format(" - CHYBA - {0}", ex.Message.ToString()));
        }
        return 0;
    }

    private static string GetProductFeature(long feature_id)
    {
        Bukimedia.PrestaSharp.Factories.ProductFeatureFactory fct = new Bukimedia.PrestaSharp.Factories.ProductFeatureFactory(Settings.Default.baseurl, Settings.Default.account, "");

        Bukimedia.PrestaSharp.Entities.product_feature pf = fct.Get(feature_id);

        if (pf != null)
            return pf.name[0].Value;

        return "";
    }

    private static string GetProductFeatureValue(long id_feature_value)
    {
        Bukimedia.PrestaSharp.Factories.ProductFeatureValueFactory fct = new Bukimedia.PrestaSharp.Factories.ProductFeatureValueFactory(Settings.Default.baseurl, Settings.Default.account, "");
        Bukimedia.PrestaSharp.Entities.product_feature_value pfl = fct.Get(id_feature_value);

        if (pfl != null)
            return pfl.value[0].Value;

        return "";
    }

    private static string GetProductOption(long product_option_value_id)
    {
        Bukimedia.PrestaSharp.Factories.ProductOptionFactory fct = new Bukimedia.PrestaSharp.Factories.ProductOptionFactory(Settings.Default.baseurl, Settings.Default.account, "");
        Bukimedia.PrestaSharp.Entities.product_option po = fct.Get(product_option_value_id);

        if (po != null)

            if (po.name.Count > 0)
            {
                return po.name[0].Value.ToString();
            }

        return "";
    }

    private static string GetProductOptionValue(long product_option_value_id)
    {
        Bukimedia.PrestaSharp.Factories.ProductOptionValueFactory fct = new Bukimedia.PrestaSharp.Factories.ProductOptionValueFactory(Settings.Default.baseurl, Settings.Default.account, "");
        Bukimedia.PrestaSharp.Entities.product_option_value pov = fct.Get(product_option_value_id);

        if (pov != null)

            return string.Format("{0}:{1}", GetProductOption(((long)pov.id_attribute_group)), pov.name[0].Value);

        return "";
    }

    private static AswLib.Tblvb GetProductVariantMySQL(bool all = true, int top = 0)
    {
        SetLog("Načítám varianty produktu mysql", false);
        AswLib.Tblvb dt = new AswLib.Tblvb();
        if (all)
            dt.NactiData(Settings.Default.cnn_priority + 1, "select * from ps_product_attribute", "ps_product_attribute");
        else if (top > 0)
            dt.NactiData(Settings.Default.cnn_priority + 1, string.Format("select TOP {0} * from ps_product_attribute", top), "ps_product_attribute");

        if (dt.Nacetldata)
        {
            SetLog(" - OK");
        }
        else
        {
            SetChyba("Chyba při načítání tabulky ps_product_attribute" + dt.Chyba);
        }

        return dt;
    }

    private static Tblvb GetProdukty(bool all = true, int top = 0, bool changes = false, DateTime dtm = new DateTime(), long produkt = 0, bool discount = false, bool active=true)
    {
        SetLog("Načítám produkty", false);
        Tblvb dt = new Tblvb();
        if (all)
            if (active)
            {
                dt.NactiData(Settings.Default.cnn_priority, "select *, (select count(id) from ProductVariant as pv where pv.Product = products.id) as Variant,  (select sum(StoreProducts.RealTotal) from StoreProducts where Product=Products.id) as Total from products where Valid=1 and Setting in (select id from ProductSettings where Internet = 1) order by Name", "Products");
            }
            else
            {

                dt.NactiData(Settings.Default.cnn_priority, "select * from Products where (Setting in (select id from ProductSettings where Internet=0)) and web_id>0", "Products");
            }
        else if (top > 0)
            dt.NactiData(Settings.Default.cnn_priority, string.Format("select TOP {0} *, (select count(id) from ProductVariant as pv where pv.Product = products.id) as Variant,  (select sum(StoreProducts.RealTotal) from StoreProducts where Product=Products.id) as Total from products where Valid=1 and Setting in (select id from ProductSettings where Internet = 1) order by Name", top), "Products");
        else if (produkt > 0)
            dt.NactiData(Settings.Default.cnn_priority, string.Format("select *, (select count(id) from ProductVariant as pv where pv.Product = products.id) as Variant,  (select sum(StoreProducts.RealTotal) from StoreProducts where Product=Products.id) as Total from products where Valid=1 and Setting in (select id from ProductSettings where Internet = 1) and id={0} order by Name", produkt), "Products");
        else if (discount)
            dt.NactiData(Settings.Default.cnn_priority, "select  *, (select count(id) from ProductVariant as pv where pv.Product = products.id) as Variant,  (select sum(StoreProducts.RealTotal) from StoreProducts where Product = Products.id) as Total  from products where id in (select distinct product from(select pv.id, spck.PriceNoVat, isnull(ids.Discount, 0) as Discount, pv.product, pv.web_id, Products.web_id as prdid, (select sum(StoreProducts.RealTotal) from StoreProducts where Variant = pv.id) as Total from ProductVariant as pv inner join packages as pck on pv.id = pck.Variant inner join StorePackages as spck on pck.id = spck.Package left join IndividualDiscounts as ids on ids.id = spck.IndividualDiscount inner join products on products.id = pv.Product where ids.Discount >0) as t ) and Setting in (select id from ProductSettings where internet = 1 and valid = 1) order by Name", "Products");
        else if (changes)
        {
            string sql = string.Format("select *, (select count(id) from ProductVariant as pv where pv.Product = products.id) as Variant, (select sum(StoreProducts.RealTotal) from StoreProducts where Product=Products.id) as Total from products where Valid=1 and Setting in (select id from ProductSettings where Internet = 1) and (id in (select itemid from changes where TableName='Products' and Ts>=dateadd(s,{0},getdate())) or web_id=0 or id in (select product from StoreProducts where lastchange >=dateadd(s,{0},getdate()))) order by Name", (int)(dtm - DateTime.Now).TotalSeconds);
            dt.NactiData(Settings.Default.cnn_priority, sql, "Products");
        }

        if (dt.Nacetldata)
        {
            SetLog(" - OK");
        }
        else
        {
            SetChyba("Chyba při načítání tabulky produkty" + dt.Chyba);
        }

        return dt;
    }

    private static AswLib.Tblvb GetProduktyNoVariant()
    {
        SetLog("Načítám produkty bez variací", false);
        AswLib.Tblvb dt = new AswLib.Tblvb();

        dt.NactiData(Settings.Default.cnn_priority, "select p.web_id, sp.PriceNoVat, ISNULL(ids.Discount,0) as Discount, p.Vat as Vat, p.ID as ID, pck.Variant as Variant from products as p inner join packages pck on pck.Product = p.id inner join StorePackages as sp on sp.Package = pck.id left join IndividualDiscounts as ids on sp.IndividualDiscount = ids.ID where p.Setting in (select id from ProductSettings where Internet =1) and p.Valid = 1  and sp.PriceNoVat>0  order by web_id, PriceNoVat", "Products");

        if (dt.Nacetldata)
        {
            SetLog(" - OK");
        }
        else
        {
            SetChyba("Chyba při načítání tabulky produktů bez variací" + dt.Chyba);
        }

        return dt;
    }

    private static string getRequeststring(string mystr)
    {
        mystr = mystr.Replace("'", "");
        System.Text.Encoding utf8 = System.Text.Encoding.UTF8;
        byte[] utf8Bytes = utf8.GetBytes(mystr);
        return string.Join(";", utf8Bytes);
    }

    private static string getRewriteFromName(string Name)
    {
        Name = Name.ToLower();
        Name = Name.Replace(" ", "-");
        Name = Name.Replace(".", "-");
        Name = Name.Replace(",", "-");
        Name = Name.Replace(":", "-");
        Name = Name.Replace(";", "-");
        Name = Name.Replace("/", "");
        Name = Name.Replace("(", "");
        Name = Name.Replace(")", "");
        Name = Name.Replace("[", "");
        Name = Name.Replace("]", "");
        Name = Name.Replace("{", "");
        Name = Name.Replace("}", "");
        Name = Name.Replace("+", "");
        Name = Name.Replace("\\", "");
        Name = Name.Replace("&", "");
        Name = Name.Replace("!", "");
        Name = Name.Replace("°", "");
        string enterek = ((char)34).ToString();
        Name = Name.Replace(enterek, "");
        Name = Name.Replace("ě", "e");
        Name = Name.Replace("š", "s");
        Name = Name.Replace("č", "c");
        Name = Name.Replace("ř", "r");
        Name = Name.Replace("ž", "z");
        Name = Name.Replace("ý", "y");
        Name = Name.Replace("á", "a");
        Name = Name.Replace("í", "i");
        Name = Name.Replace("é", "e");
        Name = Name.Replace("ň", "n");
        Name = Name.Replace("ť", "t");
        Name = Name.Replace("ď", "d");
        Name = Name.Replace("ů", "u");
        Name = Name.Replace("ú", "u");

        return Name.ToLower();
    }

    // Vytvoření update skriptu pro aktualizaci kategorii eshop mysql
    private static void GetUpdateCategoriesMySqlScript()
    {
        AswLib.Tblvb dt = GetKategorie();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (Convert.ToInt32(dt.Rows[i]["web_id"]) > 0)
                SetLog(string.Format("update ps_category set position={0}, parent={2} where id_category={1};", dt.Rows[i]["position"], dt.Rows[i]["web_id"], dt.Rows[i]["webpar_id"]));
        }
        EndApp();
    }

    private static void GetUpdateCategoriesScript()
    {
        AswLib.Tblvb dt = GetKategorie();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            SetLog(string.Format("update Categories set web_id={0}, webpar_id={1} where ID={2}", dt.Rows[i]["web_id"], dt.Rows[i]["webpar_id"], dt.Rows[i]["ID"]));
        }
        EndApp();
    }

    private static void GetUpdateImageScript()
    {
        AswLib.Tblvb dt = GetImages();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (Convert.ToInt32(dt.Rows[i]["web_id"]) > 0)
                SetLog(string.Format("update productimages set web_id={0} where id={1};", dt.Rows[i]["web_id"], dt.Rows[i]["id"]));
        }
        EndApp();
    }

    // vytvoření update scriptu mysql pro aktualizaci množství produktů
    private static void GetUpdateProductsAmountScript()
    {
        AswLib.Tblvb dt = GetProdukty();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (Convert.ToInt32(dt.Rows[i]["Variant"]) == 0)
            {
                int mnozstvi = 0;
                if (Convert.ToInt32(dt.Rows[i]["Total"]) > 0)
                    mnozstvi = Convert.ToInt32(dt.Rows[i]["Total"]);
                SetLog(string.Format("update ps_stock_available set quantity={0} where id_product={1} and id_product_attribute=0;", mnozstvi, dt.Rows[i]["web_id"]));
            }
        }
        EndApp();
    }

    private static void GetUpdateProductsPriceNoVariantScript()
    {
        AswLib.Tblvb dt = GetProduktyNoVariant();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            int tax_group = 1;
            if (Convert.ToInt32(dt.Rows[i]["Vat"]) != Settings.Default.base_vat)
                tax_group = 2;
            SetLog(string.Format("update ps_product_shop set id_tax_rules_group={0}, wholesale_price=0, price={1} where id_product={2};", tax_group, dt.Rows[i]["PriceNoVat"].ToString().Replace(',', '.'), dt.Rows[i]["web_id"]));
            //SetLog(string.Format("update ps_product_shop set id_tax_rules_group={0}, wholesale_price=0, price={1} where id_product={2};", tax_group, dt.Rows[i]["PriceNoVat"].ToString().Replace(',', '.'), dt.Rows[i]["web_id"]));
        }
        EndApp();
    }

    private static void GetUpdateProductsScript()
    {
        AswLib.Tblvb dt = GetProdukty();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            SetLog(string.Format("update Products set web_id={0} where ID={1}", dt.Rows[i]["web_id"], dt.Rows[i]["ID"]));
        }
        EndApp();
    }

    // vytvoření update scriptu mysql pro aktualizaci množství balení
    private static void GetUpdateProductsVariantAmountScript()
    {
        AswLib.Tblvb dt = GetVariantyProduktu();

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (Convert.ToInt32(dt.Rows[i]["web_id"]) > 0 && Convert.ToInt32(dt.Rows[i]["prdid"]) > 0)
            {
                int mnozstvi = 0;
                if (Convert.ToInt32(dt.Rows[i]["Total"]) > 0)
                    mnozstvi = Convert.ToInt32(dt.Rows[i]["Total"]);
                SetLog(string.Format("update ps_stock_available set quantity={0} where id_product={1} and id_product_attribute={2};", mnozstvi, dt.Rows[i]["web_id"], dt.Rows[i]["prdid"]));
            }
        }
        EndApp();
    }

    // Vytvoření update skriptu pro aktualizaci web_id produktu
    // Vytvoření update skriptu pro aktualizaci web_id kategorií
    // Vytvoření update skriptu mysql pro aktualizaci sazby dph
    private static void GetUpdateProductsVatsScript()
    {
        AswLib.Tblvb dt = GetProdukty();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            int tax_group = 1;
            if (Convert.ToInt32(dt.Rows[i]["Vat"]) != Settings.Default.base_vat)
                tax_group = 2;
            SetLog(string.Format("update ps_product_shop set id_tax_rules_group={0} where id_product={1};", tax_group, dt.Rows[i]["web_id"]));
        }
        EndApp();
    }

    private static void GetUpdateProductVariantScript()
    {
        AswLib.Tblvb dt = GetProductVariantMySQL();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            SetLog(string.Format("update ProductVariant set web_id={0} where ID={1}", dt.Rows[i]["id_product_attribute"], dt.Rows[i]["reference"]));
        }
        EndApp();
    }

    // Funkce vracející tabulku Packages
    // funkce vracející tabulku variant produktu
    private static AswLib.Tblvb GetVariantyProduktu(bool all = true, int top = 0)
    {
        SetLog("Načítám Varianty produktu", false);
        AswLib.Tblvb dt = new AswLib.Tblvb();
        if (all)
            dt.NactiData(Settings.Default.cnn_priority, "select pv.id, spck.PriceNoVat, isnull(ids.Discount,0) as Discount, pv.product, pv.web_id, Products.web_id as prdid, (select sum(StoreProducts.RealTotal) from StoreProducts where Variant=pv.id) as Total from ProductVariant as pv inner join packages as pck on pv.id = pck.Variant inner join StorePackages as spck on pck.id = spck.Package left join IndividualDiscounts as ids on ids.id = spck.IndividualDiscount inner join products on products.id = pv.Product order by product, Discount desc, total desc", "varianty");
        else if (top > 0)
            dt.NactiData(Settings.Default.cnn_priority, string.Format("select TOP {0} select pv.id, spck.PriceNoVat, isnull(ids.Discount,0) as Discount, pv.product, pv.web_id, Products.web_id as prdid, (select sum(StoreProducts.RealTotal) from StoreProducts where Variant=pv.id) as Total from ProductVariant as pv inner join packages as pck on pv.id = pck.Variant inner join StorePackages as spck on pck.id = spck.Package left join IndividualDiscounts as ids on ids.id = spck.IndividualDiscount inner join products on products.id = pv.Product order by product, Discount desc, total desc", top), "varianty");

        if (dt.Nacetldata)
        {
            SetLog(" - OK");
        }
        else
        {
            SetChyba("Chyba při načítání tabulky variant produktu" + dt.Chyba);
        }

        return dt;
    }

    private static AswLib.Tblvb GetVariantyTable(bool all = true, int top = 0)
    {
        SetLog("Načítám Varianty produktu", false);
        AswLib.Tblvb dt = new AswLib.Tblvb();
        if (all)
            dt.NactiData(Settings.Default.cnn_priority, "select * from ProductVariant order by product", "varianty");
        else if (top > 0)
            dt.NactiData(Settings.Default.cnn_priority, string.Format("select TOP {0} * from ProductVariant order by product", top), "varianty");

        if (dt.Nacetldata)
        {
            SetLog(" - OK");
        }
        else
        {
            SetChyba("Chyba při načítání tabulky variant produktu" + dt.Chyba);
        }

        return dt;
    }

    // funkce vracející categorii dle id
    private static category GetWebCatById(long id)
    {
        Bukimedia.PrestaSharp.Factories.CategoryFactory fct = new Bukimedia.PrestaSharp.Factories.CategoryFactory(Settings.Default.baseurl, Settings.Default.account, "");
        return fct.Get(id);
    }

    private static int GetWebCats(DataRow rw, int parrent)
    {
        Bukimedia.PrestaSharp.Factories.CategoryFactory fct = new Bukimedia.PrestaSharp.Factories.CategoryFactory(Settings.Default.baseurl, Settings.Default.account, "");
        SetLog(string.Format("Hledám kategorii v eshopu: {0}", rw["name"]));

        int id_category = PrestaSharpAnalyzer.GetFirstCategoryIdByName(fct, rw["name"].ToString(), Convert.ToInt32(rw["webpar_id"]));
        if (id_category > 0)
        {
            SetLog(string.Format("Nalezeno id: {0}", id_category));

            return id_category;
        }
        else
        {
            //SetLog("Kategorie nenalezena, vytvářím kategorii");

            //id_category = AddCategory(0, parrent, rw["name"].ToString(), 1, (Convert.ToBoolean(rw["valid"]) & Convert.ToBoolean(rw["internet"])), false);
            id_category = UpdateCategoryScript(0, parrent, rw["name"].ToString(), 1, (Convert.ToBoolean(rw["valid"]) & Convert.ToBoolean(rw["internet"])), false);
            if (id_category > 0)
            {
                rw["web_id"] = id_category;
                return id_category;
            }
        }
        return 0;
    }

    private static int GetWebIdProduct(DataTable dt, int id)
    {
        DataRow[] rws = dt.Select(string.Format("ID={0}", id));
        if (rws.Length > 0)
        {
            return Convert.ToInt32(rws[0]["web_id"]);
        }

        return 0;
    }

    private static DataRow GetWebIdVarinat(AswLib.Tblvb dt, int id)
    {
        DataRow[] vartbls = dt.Select(string.Format("ID={0}", id), "");
        if (vartbls.Length > 0)
            return vartbls[0];

        return null;
    }

    private static string GetWebsCats(DataTable dtcats, long id)
    {
        DataRow[] rvs = dtcats.Select(string.Format("ID={0}", id), "");

        if (rvs.Length > 0)
        {
            if (Convert.ToInt64(rvs[0]["webpar_id"]) == Settings.Default.default_catparent)
            {
                return rvs[0]["webpar_id"].ToString() + "," + rvs[0]["web_id"].ToString();
            }
            else
                return GetWebsCats(dtcats, Convert.ToInt32(rvs[0]["Parent"])) + "," + rvs[0]["web_id"].ToString();
        }
        else
            return "";
    }

    private static void CheckDir(string path)
    {
        if (!System.IO.Directory.Exists(path))
        {
            System.IO.Directory.CreateDirectory(path);
        }
    }

    private static void CheckLogDir()
    {
        string dir = string.Format("{0}\\Log", System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));
        CheckDir(dir);
    }

    private static DateTime LastSync(int days = 0)

    {
        if (days > 0)
        {
            return DateTime.Now.AddDays(-days);
        }

        DateTime dtm = DateTime.Now.AddDays(-7);
        string dir = string.Format("{0}\\Log", System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));
        string[] soubory = System.IO.Directory.GetFiles(dir);

        SetLog(string.Format("Počet nalezených souborů: {0}", soubory.Length));
        for (int i = 0; i <= soubory.Length - 1; i++)
        {
            if (soubory[i].ToString().Contains("_0_"))
            {
                if (dtm < System.IO.File.GetCreationTime(soubory[i]))
                    dtm = System.IO.File.GetCreationTime(soubory[i]);
            }
        }
        return dtm;
    }

    // Aktualizace množství balení PHP
    // aktualizace web_id u balení produktu
    //
    private static void LogInsertTemplateSQL(string tbl)
    {
        StartApp("Mysql");
        Tblvb dt = new Tblvb();
        dt.NactiData(Settings.Default.cnn_priority + 1, string.Format("Select  * from {0}", tbl), tbl);
        if (dt.Nacetldata)
        {
            if (dt.Rows.Count > 0)
            {
                string sql = "";
                sql += string.Format("insert into {0} (", tbl);
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (i > 0)
                        sql += ", ";
                    sql += dt.Columns[i].ColumnName.ToString();
                }
                sql += ") Values (";
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (i > 0)
                        sql += ", ";
                    if (dt.Columns[i].DataType.ToString() == "System.String")
                    {
                        sql += string.Format("'{0}'", dt.Columns[i].ColumnName.ToString());
                    }
                    else
                        sql += dt.Rows[0][i].ToString().Replace(',', '.');
                }
                sql += ")";
                SetLog(sql);
            }
        }
        EndApp();
    }

    private static void ReadAllLanguages()

    {
        StartApp("Výpis jazyků");
        Bukimedia.PrestaSharp.Factories.LanguageFactory fct = new Bukimedia.PrestaSharp.Factories.LanguageFactory(Settings.Default.baseurl, Settings.Default.account, "");

        SetLog("Načítám jazyky", false);
        List<long> lst = fct.GetIds();
        if (lst != null)
        {
            SetLog(string.Format(" CELKEM: {0}", lst.Count));

            for (int i = 0; i <= lst.Count - 1; i++)
            {
                Bukimedia.PrestaSharp.Entities.language lng = fct.Get(lst[i]);

                if (lng.id > 0)
                {
                    SetLog(string.Format("{0}-{1} [active:{2}]", lng.id, lng.name, lng.active));
                }
            }
        }

        EndApp();
    }

    private static void ReadAllWebCategories()
    {
        StartApp("Výpis kategorií");
        Bukimedia.PrestaSharp.Factories.CategoryFactory fct = new Bukimedia.PrestaSharp.Factories.CategoryFactory(Settings.Default.baseurl, Settings.Default.account, "");

        SetLog("Načítám kategorie", false);
        List<long> lst = fct.GetIds();
        if (lst != null)
        {
            SetLog(string.Format(" CELKEM: {0}", lst.Count));

            for (int i = 0; i <= lst.Count - 1; i++)
            {
                Bukimedia.PrestaSharp.Entities.category cat = fct.Get(lst[i]);

                if (cat.id > 0)
                {
                    SetLog(string.Format("{0}-{1} [root:{2} shopdefault: {3}]", cat.id, cat.name[0].Value, cat.is_root_category, cat.id_shop_default));
                }
            }
        }

        EndApp();
    }

    private static void ReadAllWebProducts()
    {
        StartApp("Výpis produktů");
        Bukimedia.PrestaSharp.Factories.ProductFactory fct = new Bukimedia.PrestaSharp.Factories.ProductFactory(Settings.Default.baseurl, Settings.Default.account, "");

        SetLog("Načítám produkty", false);
        List<long> lst = fct.GetIds();
        if (lst != null)
        {
            SetLog(string.Format(" CELKEM: {0}", lst.Count));

            for (int i = 0; i <= lst.Count - 1; i++)
            {
                Bukimedia.PrestaSharp.Entities.product prd = fct.Get(lst[i]);

                if (prd.id > 0)
                {
                    SetLog(string.Format("{0}-{1}, cena: {2}, reference: {3}, link: {4}", prd.id, prd.name[0].Value, prd.price, prd.reference, prd.link_rewrite[0].Value));
                    ReadProductCategory((long)prd.id_category_default);
                    ReadProductImage((long)prd.id);

                    ReadProductCombination((long)prd.id);
                    ReadProductSpecialPrice((long)prd.id);
                    ReadProductAssociation(prd.associations);
                    SetLog("------------------------------------------------------------------------");
                }
            }
        }

        EndApp();
    }

    // Revize
    private static void ReadLanguages(int id)
    {
        Bukimedia.PrestaSharp.Factories.LanguageFactory fct = new Bukimedia.PrestaSharp.Factories.LanguageFactory(Settings.Default.baseurl, Settings.Default.account, "");
        Bukimedia.PrestaSharp.Entities.language lng = fct.Get(id);
        if (lng != null)
        {
            SetLog(lng.name);
        }
    }

    private static void ReadProductAssociation(Bukimedia.PrestaSharp.Entities.AuxEntities.AssociationsProduct asc)
    {
        if (asc != null)
        {
            if (asc.product_features != null)
            {
                if (asc.product_features.Count > 0)
                {
                    SetLog(string.Format("Vlastností: {0}", asc.product_features.Count));
                    for (int a = 0; a < asc.product_features.Count; a++)
                    {
                        SetLog(string.Format("{0}-{1}", GetProductFeature(asc.product_features[a].id), GetProductFeatureValue(asc.product_features[a].id_feature_value)));
                    }
                }
            }

            if (asc.product_option_values != null)
            {
                if (asc.product_option_values.Count > 0)
                {
                    SetLog(string.Format("Nastavení: {0}", asc.product_option_values.Count));
                    for (int a = 0; a < asc.product_option_values.Count; a++)
                    {
                        SetLog(string.Format("{0}", GetProductOptionValue(asc.product_option_values[a].id)));
                    }
                }
            }
        }
    }

    private static void ReadProductCategory(long id)
    {
        Bukimedia.PrestaSharp.Factories.CategoryFactory fct = new Bukimedia.PrestaSharp.Factories.CategoryFactory(Settings.Default.baseurl, Settings.Default.account, "");

        SetLog(string.Format("Kategorie: {0}", GetCategoryTree(fct, id)));
    }

    // Odstranění všech balení produktu
    // Výpis balení produktu
    private static void ReadProductCombination(long prdid)
    {
        Bukimedia.PrestaSharp.Factories.CombinationFactory fct = new Bukimedia.PrestaSharp.Factories.CombinationFactory(Settings.Default.baseurl, Settings.Default.account, "");

        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("id_product", prdid.ToString());

        List<Bukimedia.PrestaSharp.Entities.combination> lst = fct.GetByFilter(dic, "", "");
        if (lst.Count > 0)
        {
            SetLog(string.Format("Nalezeno {0} kombinací", lst.Count));

            for (int i = 0; i < lst.Count; i++)
            {
                SetLog(string.Format("Kombinace id: {0}, cena: {1}, množství: {2}", lst[i].id, lst[i].price, lst[i].quantity), false);
                ReadProductCombinationAssociation(lst[i].associations);
                SetLog("");
            }
        }
    }

    private static void ReadProductCombinationAssociation(Bukimedia.PrestaSharp.Entities.AuxEntities.AssociationsCombination asc)
    {
        if (asc != null)
        {
            if (asc.product_option_values != null)
            {
                if (asc.product_option_values.Count > 0)
                {
                    for (int a = 0; a < asc.product_option_values.Count; a++)
                    {
                        SetLog(string.Format(" {0}", GetProductOptionValue(asc.product_option_values[a].id)), false);
                    }
                }
            }
        }
    }

    private static void ReadProductImage(long prdid)
    {
        Bukimedia.PrestaSharp.Factories.ImageFactory fct = new Bukimedia.PrestaSharp.Factories.ImageFactory(Settings.Default.baseurl, Settings.Default.account, "");

        List<Bukimedia.PrestaSharp.Entities.FilterEntities.declination> lst = fct.GetProductImages(prdid);
        if (lst.Count > 0)
        {
            SetLog(string.Format("Nalezeno {0} obrázků", lst.Count));

            for (int i = 0; i < lst.Count; i++)
            {
                SetLog(string.Format("Obrázek id: {0}", lst[i].id));
            }
        }
    }

    private static void ReadProductSpecialPrice(long prdid)
    {
        Bukimedia.PrestaSharp.Factories.SpecificPriceFactory fct = new Bukimedia.PrestaSharp.Factories.SpecificPriceFactory(Settings.Default.baseurl, Settings.Default.account, "");

        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("id_product", prdid.ToString());
        List<Bukimedia.PrestaSharp.Entities.specific_price> lst = fct.GetByFilter(dic, "", "");
        if (lst.Count > 0)
        {
            SetLog(string.Format("Nalezeno {0} speciálních cen", lst.Count));

            for (int i = 0; i < lst.Count; i++)
            {
                SetLog(string.Format("Speciální cena id: {0}", lst[i].id));
            }
        }
    }

    // funkce pro úpravu velikosti obrázku
    private static System.Drawing.Image ResizeImage(System.Drawing.Image img, int width, int height, int orgwidth, int orgheight)
    {
        System.Drawing.Bitmap b = new System.Drawing.Bitmap(width, height);

        decimal wscale = width / Convert.ToDecimal(orgwidth);
        decimal hscale = height / Convert.ToDecimal(orgheight);
        int mwidth = width;
        int mheight = height;
        if (wscale < hscale)
        {
            mheight = (int)(wscale * orgheight);
        }
        else if (wscale > hscale)
        {
            mwidth = (int)(hscale * orgwidth);
        }

        using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage((System.Drawing.Image)b))
        {
            g.Clear(System.Drawing.Color.White);
            int x = Convert.ToInt32((b.Width - mwidth) / 2);
            int y = Convert.ToInt32((b.Height - mheight) / 2);
            g.DrawImage(img, x, y, mwidth, mheight);
        }
        return (System.Drawing.Image)b;
    }

    private static void SaveFileToFTP(string file, string destfolder)
    {
        MakeFTPDir(Settings.Default.ftp_srv, destfolder, Settings.Default.ftp_user, Settings.Default.ftp_pwd, null);
        SetLog("Ukládám soubor na FTP");
        string filename = System.IO.Path.GetFileName(file);
        string target = string.Format("ftp://{0}/{1}/{2}", Settings.Default.ftp_srv, destfolder, filename);
        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(target);
        request.Credentials = new NetworkCredential(Settings.Default.ftp_user, Settings.Default.ftp_pwd);
        request.Method = WebRequestMethods.Ftp.UploadFile;
        request.UseBinary = true;
        request.UsePassive = true;
        request.KeepAlive = true;

        using (System.IO.Stream fileStream = System.IO.File.OpenRead(file))
        {
            using (System.IO.Stream ftpStream = request.GetRequestStream())
            {
                byte[] buffer = new byte[10240];
                int read;
                while ((read = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ftpStream.Write(buffer, 0, read);
                    SetLog(string.Format("Nahráno {0} bytů", fileStream.Position));
                }
            }
        }
    }

    private static bool SaveImg(int prdid, int prd_attr_id, int imgid, byte[] rowimg, bool toftp = true)
    {
        string formaty = "cart_default,80,80;small_default,98,98;medium_default,125,125;home_default,250,250;large_default,458,458;thickbox_default,800,800";
        string[] frm = formaty.Split(';');
        System.Drawing.Image img = GetImageFromBytes(rowimg);
        string dir = string.Format("{0}\\img", System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));
        CheckDir(dir);
        string path = GetPathImg(dir, imgid);

        CheckDir(path);
        DeleteDir(path);

        img.Save(string.Format("{0}\\{1}.jpg", path, imgid), System.Drawing.Imaging.ImageFormat.Jpeg);
        string ftppath = GetPathImg("", Convert.ToInt32(imgid));
        string fpath = string.Format("{0}\\{1}.jpg", path, imgid);
        System.IO.File.Copy(dir + "\\filetype", path + "\\filetype", true);
        System.IO.File.Copy(dir + "\\index.php", path + "\\index.php", true);

        if (toftp)
        {
            try
            {
                SaveFileToFTP(fpath, string.Format("www/img/p/{0}", ftppath));
            }
            catch (Exception exs)
            {
                SetLog("Nahrání na FTP - chyba" + exs.Message.ToString());
                return false;
            }
        }
        for (int a = 0; a < frm.Length; a++)
        {
            string[] imgtyp = frm[a].Split(',');
            System.Drawing.Image myig = ResizeImage(img, Convert.ToInt32(imgtyp[1]), Convert.ToInt32(imgtyp[2]), img.Width, img.Height);
            string filepath = string.Format("{2}\\{0}-{1}.jpg", imgid, imgtyp[0], path);
            string filepath2 = string.Format("{0}\\filetype", dir);
            string filepath3 = string.Format("{0}\\index.php", dir);
            myig.Save(filepath, System.Drawing.Imaging.ImageFormat.Jpeg);
            ftppath = ftppath.Replace("\\", "/");
            if (toftp)
            {
                try
                {
                    SaveFileToFTP(filepath, string.Format("www/img/p/{0}", ftppath));
                    SaveFileToFTP(filepath2, string.Format("www/img/p/{0}", ftppath));
                    SaveFileToFTP(filepath3, string.Format("www/img/p/{0}", ftppath));
                }
                catch (Exception exs)
                {
                    SetLog("Nahrání na FTP - chyba" + exs.Message.ToString());
                    return false;
                }
            }
        }

        return true;
    }

    private static string SaveLog()
    {
        CheckLogDir();
        string dir = string.Format("{0}\\Log", System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));
        string file = string.Format("{0}\\log_{1}_{2}.log", dir, _cislo_app, DateTime.Now.ToFileTimeUtc());
        try
        {
            SetLog("Ukládám log", false);
            System.IO.File.WriteAllText(file, _log, System.Text.Encoding.Default);
            SetLog(" - OK");
            return file;
        }
        catch (Exception ex)
        {
            SetChyba(string.Format(" - CHYBA: {0}", ex.Message.ToString()));
            return "";
        }
    }

    // funkce vracející web_id produktu
    // výpis slev
    // funkce vracející název název vlastnosti prestasharp
    // funkce vracející hodnotu vlastnosti prestasharp
    // funkce vracející název vlastnosti prestasharp
    // funkce vracející hodnotu vlastnosti prestasharp
    // výpis asociace
    // Výpis asociace balení prestasharp
    // Nastavení množství prestasharp
    private static stock_available SetAmountProductCombination(long prdid, long prd_attr_id, int mnozstvi)
    {
        Bukimedia.PrestaSharp.Factories.StockAvailableFactory fct = new Bukimedia.PrestaSharp.Factories.StockAvailableFactory(Settings.Default.baseurl, Settings.Default.account, "");
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic.Add("id_product", prdid.ToString());
        dic.Add("id_product_attribute", prd_attr_id.ToString());
        List<Bukimedia.PrestaSharp.Entities.stock_available> lst = fct.GetByFilter(dic, "", "");
        stock_available stck = null;
        if (lst != null)
        {
            if (lst.Count > 0)
            {
                stck = lst[0];
                stck.quantity = mnozstvi;
                stck.id_shop = 1;
                stck.out_of_stock = 2;
                fct.Update(stck);
                return stck;
            }
        }
        if (stck == null)
        {
            lst = new List<stock_available>();

            stck = new stock_available();
            stck.id_product = prdid;
            stck.id_product_attribute = prd_attr_id;
            stck.quantity = mnozstvi;
            stck.id_shop = 1;
            stck.out_of_stock = 2;
            lst.Add(stck);

            lst = fct.AddList(lst);
            return stck;
        }
        return null;
    }

    private static void SetAssociationProduct()
    {
        StartApp("Asocicace produkty a kategorie");
        AswLib.Tblvb tbass = GetKategorieProduct();
        AswLib.Tblvb tbprod = GetProdukty();
        AswLib.Tblvb tblwebcat = GetKategorie();
        for (int i = 0; i < tbprod.Rows.Count; i++)
        {
            string webcats = GetWebsCats(tblwebcat, Convert.ToInt64(tbprod.Rows[i]["Category"]));
            SetLog(string.Format("Produkt {0} / {1}", webcats, tbprod.Rows[i]["name"]));
            string[] cats = webcats.Split(',');
            for (int a = 0; a < cats.Length; a++)
            {
                DataRow rw = tbass.NewRow();
                rw["id_product"] = tbprod.Rows[i]["web_id"];
                rw["id_category"] = cats[a];
                int pozice = Convert.ToInt32(tbprod.Rows[i]["PlaceNo"]);
                if (pozice < 0)
                    pozice = 0;
                rw["position"] = pozice;

                tbass.Rows.Add(rw);
            }
        }
        SetLog("Aktualizuji data", false);
        tbass.AktualizujData();
        if (tbass.UpdateOk)
        {
            SetLog(" - OK");
        }
        else
            SetChyba(" - CHYBA: " + tbass.Chyba);

        EndApp();
    }

    private static void SetChanges(int oznameni, int days = 0, long produkt = 0, bool discount = false, bool updcat = true, int starti = 0, int endi = 100000)
    {
        StartApp("Synchronizace změn");
        AddComplet(false, false, false, true, starti, endi, false, 0, true, days, produkt, discount, updcat);
        Settings.Default.last_sync = DateTime.Now;
        EndApp(true, oznameni);
    }

    private static void SetChyba(string chyba, bool line = true)
    {
        _chyb += 1;
        SetLog(chyba, line);
    }

    private static void SetLevel(AswLib.Tblvb dt)
    {
        SetLog("Analzuji strukturu");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            dt.Rows[i]["level"] = GetLevel(dt, Convert.ToInt32(dt.Rows[i]["Parent"]), 0);
        }
    }

    private static void SetLog(string msg, bool line = true)
    {
        _log += msg;

        if (line)
        {
            _log += System.Environment.NewLine;
            Console.WriteLine(msg);
        }
        else
        {
            Console.Write(msg);
        }
    }

    // ukončení aplikace
    // Výpis všech kategorií v eshopu presta
    // Výpis všech produktů v eshopu
    // Výpis asociace kategorie - stromu
    // Výpis kategorie produktu
    // Odstranění vazby kategoríí
    private static void SetNullWebCategories()
    {
        StartApp("Odstranění vazby na TRANSWARE iNET");
        SetLog("Kategorie");
        AswLib.Pripojeni.SQL prip = new AswLib.Pripojeni.SQL(Settings.Default.cnn_priority);
        prip.Cmd.CommandText = "Update Categories set web_id=0, webpar_id=0";
        prip.Cmd.CommandType = CommandType.Text;
        prip.Exec();
        if (prip.SQLAno)
        {
            SetLog("Příkaz byl úspěšně proveden");
        }
        else
        {
            SetChyba("Během provedení příkazu došlo k následující chybě: " + prip.Chyba);
        }
        SetLog("Produkty");
        prip.Cmd.CommandText = "Update Products set web_id=0";
        prip.Cmd.CommandType = CommandType.Text;
        prip.Exec();
        if (prip.SQLAno)
        {
            SetLog("Příkaz byl úspěšně proveden");
        }
        else
        {
            SetChyba("Během provedení příkazu došlo k následující chybě: " + prip.Chyba);
        }
        EndApp();
    }

    private static void SetProductImage(long id, long prdid, AswLib.Tblvb dt, long prd_attr_id = 0)
    {
        Bukimedia.PrestaSharp.Factories.ImageFactory fct = new Bukimedia.PrestaSharp.Factories.ImageFactory(Settings.Default.baseurl, Settings.Default.account, "");
        bool existsimages = false;
        try
        {
            List<Bukimedia.PrestaSharp.Entities.FilterEntities.declination> lst = fct.GetProductImages(prdid);
            if (lst != null)
            {
                existsimages = (lst.Count > 0);
            }
        }
        catch (Exception ex)
        {
            int maximg = GetExecuteSQLId(string.Format("11|{0}", prdid));
            existsimages = (maximg > 0);
        }

        if (!existsimages)
        {
            if (prd_attr_id == 0)
            {
                DataView view = new DataView(dt, string.Format("Product={0}", id), "", DataViewRowState.OriginalRows);
                if (view.Count > 0)
                {
                    SetLog(string.Format("Nalezeno {0} obrázků", view.Count));
                    for (int i = 0; i < view.Count; i++)
                    {
                        SetLog(string.Format("Náhrávám {0}. obrázek", i + 1), false);
                        try
                        {
                            byte[] byt = (byte[])view[i]["Image"];
                            long myimg = fct.AddProductImage(prdid, byt);

                            SetLog(string.Format(" - OK id: {0}", myimg));
                        }
                        catch (Exception ex)
                        {
                            string param = string.Format("10|{0}|{1}|{2}", prdid, (i == 0) ? 1 : 0, Settings.Default.default_lng);
                            int imgid = GetExecuteSQLId(param);
                            //bool exec_shop = ExecuteSQL(string.Format("5 {0} {1} {2} {3} {4}", prdid, reference, price.ToString().Replace(',', '.'), quantity, isdefaulton ? 1 : 0));
                            if (imgid > 0)
                            {
                                string formaty = "cart_default,80,80;small_default,98,98;medium_default,125,125;home_default,250,250;large_default,458,458;thickbox_default,800,800";
                                string[] frm = formaty.Split(';');
                                System.Drawing.Image img = GetImageFromBytes((byte[])view[i]["Image"]);
                                for (int a = 0; a < frm.Length; a++)
                                {
                                    string[] imgtyp = frm[a].Split(',');
                                    System.Drawing.Image myig = ResizeImage(img, Convert.ToInt32(imgtyp[1]), Convert.ToInt32(imgtyp[2]), img.Width, img.Height);
                                    string dir = string.Format("{0}\\img", System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));
                                    CheckDir(dir);
                                    string path = GetPathImg(dir, imgid);

                                    CheckDir(path);
                                    string filepath = string.Format("{2}\\{0}-{1}.jpg", imgid, imgtyp[0], path);
                                    string filepath2 = string.Format("{0}\\filetype", dir);
                                    string filepath3 = string.Format("{0}\\index.php", dir);

                                    myig.Save(filepath, System.Drawing.Imaging.ImageFormat.Jpeg);

                                    string ftppath = GetPathImg("", Convert.ToInt32(imgid));
                                    ftppath = ftppath.Replace("\\", "/");
                                    try
                                    {
                                        SaveFileToFTP(filepath, string.Format("www/img/p/{0}", ftppath));
                                        SaveFileToFTP(filepath2, string.Format("www/img/p/{0}", ftppath));
                                        SaveFileToFTP(filepath3, string.Format("www/img/p/{0}", ftppath));
                                    }
                                    catch (Exception exs)
                                    {
                                        SetLog("Nahrání na FTP - chyba" + exs.Message.ToString());
                                    }

                                    img.Save(string.Format("{0}\\{1}.jpg", path, imgid), System.Drawing.Imaging.ImageFormat.Jpeg);
                                    if (System.IO.File.Exists(path + "\\filetype"))
                                    {
                                        System.IO.File.Copy("C:\\microdata\\img\\filetype", path + "\\filetype");
                                        System.IO.File.Copy("C:\\microdata\\img\\index.php", path + "\\index.php");
                                    }
                                }

                                SetLog(string.Format(" - OK id: {0}, product_id: {1}", imgid, prdid));
                            }
                            else
                            {
                                SetChyba(string.Format(" - CHYBA: {0}", ex.Message.ToString()));
                            }
                        }
                    }
                }
            }
        }

        //else
        //{
        //    DataView view = new DataView(dt, string.Format("Product={0} and Variant={1}", prdid, prd_attr_id), "", DataViewRowState.OriginalRows);
        //    if (view.Count > 0)
        //    {
        //        SetLog(string.Format("Nalezeno {0} obrázků", view.Count));
        //        for (int i = 0; i < view.Count; i++)
        //        {
        //            SetLog(string.Format("Náhrávám {0}. obrázek"), false);
        //            try
        //            {
        //                long myimg = 0;// fct.ad(prdid, (byte[])view[i]["Image"]);
        //                SetLog(string.Format(" - OK id: {0}", myimg));
        //            }
        //            catch (Exception ex)
        //            {
        //                SetChyba(string.Format(" - CHYBA: {0}", ex.Message.ToString()));
        //            }
        //        }
        //    }

        //}
    }

    // Aktualizace obrázku produktu

    private static void SetProductShopPrice(long prid, decimal price, long category = 0)
    {
        AswLib.Pripojeni.SQL prip = new AswLib.Pripojeni.SQL(Settings.Default.cnn_priority + 1);
        prip.Cmd.CommandText = string.Format("update ps_product_shop set price={0} where id_product={0}", prid);
        prip.Cmd.CommandType = CommandType.Text;
        prip.Exec();
        if (prip.SQLAno)
            SetLog(" - OK");
        else
            SetChyba(" - chyba: " + prip.Chyba);
    }

    //
    private static void SetWebCats(AswLib.Tblvb dt, bool updatecat = false)
    {
        for (int i = 0; i <= dt.Rows.Count - 1; i++)
        {
            SetLog(string.Format("Načítám kategorii {0}/{1}: {2}, level:{3}", i + 1, dt.Rows.Count, dt.Rows[i]["name"], dt.Rows[i]["level"]));
            if (Convert.ToInt32(dt.Rows[i]["web_id"]) == 0)
            {
                int parent = GetParentWebId(dt, Convert.ToInt32(dt.Rows[i]["parent"]));
                int id_category = GetWebCats(dt.Rows[i], parent);
                if (id_category > 0)
                {
                    dt.Rows[i]["web_id"] = id_category;
                }
            }
            else if (updatecat && Convert.ToInt32(dt.Rows[i]["web_id"]) > Settings.Default.default_catparent)
            {
                UpdateCategoryScript(Convert.ToInt32(dt.Rows[i]["web_id"]), Convert.ToInt32(dt.Rows[i]["webpar_id"]), dt.Rows[i]["name"].ToString(), Convert.ToInt32(dt.Rows[i]["position"]), (Convert.ToBoolean(dt.Rows[i]["valid"]) & Convert.ToBoolean(dt.Rows[i]["internet"])), false);
            }
        }
        SetLog("Ukládám tabulku kategorí", false);
        dt.AktualizujData();
        if (dt.UpdateOk)
        {
            SetLog(" - OK");
            dt.NactiData();
            SetWebParCats(dt);
        }
        else
            SetChyba(" - CHYBA: " + dt.Chyba);
    }

    // Nastavení parent id
    private static void SetWebParCats(AswLib.Tblvb dt, int prntid = 2)
    {
        foreach (DataRow rw in dt.Rows)
        {
            if (!object.ReferenceEquals(rw["parent"], System.DBNull.Value))
            {
                rw["webpar_id"] = GetParentWebId(dt, Convert.ToInt32(rw["parent"]));
            }
            else
            {
                rw["webpar_id"] = Settings.Default.default_catparent;
            }
        }
        dt.AktualizujData();
        if (dt.UpdateOk)
        {
            SetLog(" - OK");
        }
        else
            SetChyba(" - CHYBA: " + dt.Chyba);
    }

    private static void StartApp(string app)
    {
        _dtm = DateTime.Now;
        _chyb = 0;
        _app = app;
        SetLog(string.Format("Zahajuji aplikaci [{0}]: {1}", _app, _dtm.ToString()), true);
    }

    private static void UpdateAllProductImage(bool toftp = true, bool rewrite = false)
    {
        StartApp("Aktualizace všech obrázků");
        AswLib.Tblvb dt = GetImages();
        AswLib.Tblvb dtprod = GetProdukty();
        AswLib.Tblvb dtvar = GetVariantyProduktu();
        int myid = 0;

        int maxid = 0;
        bool cover = false;
        int prdid = 0;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            SetLog(string.Format("Zpracovávám obrázek: {0}/{1}", i + 1, dt.Rows.Count));
            if (Convert.ToInt32(dt.Rows[i]["web_id"]) > 0 && !rewrite)
                continue;
            if (myid != Convert.ToInt32(dt.Rows[i]["product"]))
            {
                cover = true;
                myid = Convert.ToInt32(dt.Rows[i]["product"]);
                maxid = 0;
                prdid = GetWebIdProduct(dtprod, Convert.ToInt32(dt.Rows[i]["product"]));
            }

            if (prdid == 0)
                continue;
            int prd_attr_id = 0;
            if (Convert.ToInt32(dt.Rows[i]["variant"]) > 0)
                prd_attr_id = GetWebIdProduct(dtvar, Convert.ToInt32(dt.Rows[i]["variant"]));
            string param = string.Format("19|{0}|{1}|{2}", prdid, prd_attr_id, maxid);
            int imgid = GetExecuteSQLId(param);

            maxid = Math.Max(maxid, imgid);
            if (imgid == 0)
            {
                param = string.Format("10|{0}|{1}|{2}|{3}", prdid, cover ? 1 : 0, Settings.Default.default_lng, prd_attr_id);
                imgid = GetExecuteSQLId(param);
            }

            //bool exec_shop = ExecuteSQL(string.Format("5 {0} {1} {2} {3} {4}", prdid, reference, price.ToString().Replace(',', '.'), quantity, isdefaulton ? 1 : 0));
            if (imgid > 0)

            {
                dt.Rows[i]["web_id"] = imgid;

                maxid = Math.Max(maxid, imgid);
                if (SaveImg(prdid, prd_attr_id, imgid, (byte[])dt.Rows[i]["Image"], toftp))
                {
                    SetLog(string.Format(" - OK id: {0}, product_id: {1}, product_attribute_id: {2}", imgid, prdid, prd_attr_id));
                }
            }
            else
            {
                SetChyba(" - CHYBA obrázek nevytvořen");
                SetLog("");
                SetLog(param);
                SetLog("");
            }
        }
        SetLog("Aktualizuji data", false);
        dt.AktualizujData();
        if (dt.UpdateOk)
        {
            SetLog(" - OK");
        }
        else
        {
            SetChyba(" - CHYBA " + dt.Chyba);
        }
        EndApp();
    }

    private static bool UpdateAllZeroAmountProduct(int quantity = 1)
    {
        SetLog("Spouštím příkaz", false);
        AswLib.Pripojeni.SQL prip = new AswLib.Pripojeni.SQL(Settings.Default.cnn_priority + 1);
        prip.Cmd.CommandText = string.Format("Update ps_stock_available set quantity={0} where quantity=0", quantity);
        prip.Cmd.CommandType = CommandType.Text;
        prip.Exec();
        if (prip.SQLAno)
        {
            SetLog(" - OK");
            return true;
        }
        else
        {
            SetChyba(" - CHYBA: " + prip.Chyba);
            return false;
        }
    }

    // aktualizace kategorie - bez reference
    private static void UpdateCategory(int web_id, int parrent, string name, int position, bool active, bool root)
    {
        Bukimedia.PrestaSharp.Factories.CategoryFactory fct = new Bukimedia.PrestaSharp.Factories.CategoryFactory(Settings.Default.baseurl, Settings.Default.account, "");
        Bukimedia.PrestaSharp.Entities.category cat = fct.Get(web_id);
        if (cat != null)
        {
            cat.name[0].Value = name;
            cat.link_rewrite[0].Value = getRewriteFromName(name);
            cat.position = position;
            cat.active = (active ? 1 : 0);
            cat.is_root_category = (root ? 1 : 0);
            cat.id_parent = parrent;

            try
            {
                fct.Update(cat);

                SetLog("Kategorie byla úspěšně aktualizována id");
            }
            catch (Exception ex)
            {
                SetChyba(string.Format("Aktualizace kategorie se nazdařilo !!!: {0}", ex.Message.ToString()));
            }
        }
    }

    private static int UpdateCategoryScript(int web_id, int parrent, string name, int position, bool active, bool root)
    {
        // Insert Kategorie    $id_category,$id_parent,$position, $root, $name, $link, $lang
        if (web_id == 0)
        {
            SetLog("Kategorie nenalezena, vytvářím kategorii", false);
            string param = string.Format("15|{0}|{1}|{2}|{3}|{4}|{5}|{6}", web_id, parrent, position, root ? 1 : 0, getRequeststring(name), getRequeststring(getRewriteFromName(name)), Settings.Default.default_lng);
            SetLog(param);
            web_id = GetExecuteSQLId(param);
            //bool exec_shop = ExecuteSQL(string.Format("5 {0} {1} {2} {3} {4}", prdid, reference, price.ToString().Replace(',', '.'), quantity, isdefaulton ? 1 : 0));
            if (web_id > 0)
            {
                SetLog(" OK, id: " + web_id.ToString());
                return web_id;
            }
            else
                SetLog(" CHYBA");
        }
        if (web_id > 0)
        {
            SetLog("Kategorie nalezena, aktualizuji kategorii", false);
            string param = string.Format("16|{0}|{1}|{2}|{3}|{4}|{5}|{6}", web_id, parrent, position, root ? 1 : 0, getRequeststring(name), getRequeststring(getRewriteFromName(name)), Settings.Default.default_lng);
            SetLog(param);
            if (ExecuteSQL(param))
            {
                SetLog(" OK, id: " + web_id.ToString());
                return web_id;
            }
            else
                SetLog(" CHYBA");
        }

        return 0;
    }

    // funkce pro převod byte na obrázek
    // uložení souboru na FTP
    // Vytvoření update scriptu mysql pro aktualizace web_id obrázků
    // vytvoření update scriptu pro aktualizace cen mysql bez variant s nulovou cenou
    private static void UpdatePriceMysql()
    {
        StartApp("Aktualizace cen produktů del mysql bez variant s nulovou cenou");
        AswLib.Tblvb tblnovar = GetProduktyNoVariant();

        AswLib.Tblvb dt = new AswLib.Tblvb();
        dt.NactiData(Settings.Default.cnn_priority + 1, "SELECT * FROM `presta16`.`ps_product_shop` where price = 0 and id_product not in (select id_product from ps_product_attribute)", "ps_product_shop");
        if (dt.Nacetldata)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                long web_id = Convert.ToInt64(dt.Rows[i]["id_product"]);
                DataRow[] rvdisc = tblnovar.Select(string.Format("web_id={0}", dt.Rows[i]["id_product"]), "");
                if (rvdisc.Length > 0)
                {
                    AktualizujCenuProduktu(web_id, Convert.ToDecimal(rvdisc[0]["PriceNoVat"]));
                    if (Convert.ToDecimal(rvdisc[0]["Discount"]) > 0)
                    {
                        decimal sleva = Convert.ToDecimal(rvdisc[0]["Discount"]) / 100;
                        AddProductSpecialPrice(web_id, 0, sleva);
                    }
                }
            }
        }

        EndApp();
    }

    // nastavení asociace produktu
    // Funkce vracející asociace kategorii text oddělený čárkou
    // funkce vracející řádek tabulky variant
    // aktualizace množství na nulu MySQL
    // Aktualizace ceny produktu bez variant prestasharp
    private static void UpdatePriceProductZeroVariant()
    {
        StartApp("Aktualizace cen produktů bez variant");

        SetLog("Načítám tabulků produktů bez variant");
        AswLib.Tblvb dt = GetProduktyNoVariant();

        if (dt != null)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i % 20 == 0)
                {
                    SaveLog();
                    System.Threading.Thread.Sleep(100);
                }

                Bukimedia.PrestaSharp.Factories.ProductFactory fct = new Bukimedia.PrestaSharp.Factories.ProductFactory(Settings.Default.baseurl, Settings.Default.account, "");
                Bukimedia.PrestaSharp.Entities.product myprd = fct.Get(Convert.ToInt64(dt.Rows[i]["web_id"]));
                if (myprd != null)
                {
                    SetLog(string.Format("Aktualizuji cenu {0}/{1}", i + 1, dt.Rows.Count), false);
                    myprd.price = Math.Round(Convert.ToDecimal(dt.Rows[i]["PriceNoVat"]), 2);
                    myprd.id_tax_rules_group = 1;
                    try
                    {
                        fct.Update(myprd);
                        SetLog(" - OK ");
                        DeleteProductSpecialPrice((long)myprd.id);
                        if (Convert.ToDecimal(dt.Rows[i]["Discount"]) > 0)

                        {
                            decimal sleva = Convert.ToDecimal(dt.Rows[i]["Discount"]) / 100;
                            AddProductSpecialPrice((long)myprd.id, 0, sleva);
                        }
                    }
                    catch (Exception ex)
                    {
                        SetChyba(" - CHYBA: " + ex.Message.ToString());
                    }
                }
            }
        }
        EndApp();
    }

    // // Aktualizace ceny produktu bez variant MySQL
    private static void UpdatePriceProductZeroVariantMysql()
    {
        StartApp("Aktualizace cen produktů bez variant");

        SetLog("Načítám tabulků produktů bez variant");
        AswLib.Tblvb dt = GetProduktyNoVariant();

        if (dt != null)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (i % 20 == 0)
                {
                    SaveLog();
                    //System.Threading.Thread.Sleep(100);
                }
                long id = Convert.ToInt64(dt.Rows[i]["web_id"]);

                if (id > 0)
                {
                    SetLog(string.Format("Aktualizuji cenu {0}/{1}", i + 1, dt.Rows.Count), false);

                    try
                    {
                        SetProductShopPrice(id, Math.Round(Convert.ToDecimal(dt.Rows[i]["PriceNoVat"]), 2));
                        if (dt.Rows[i]["Discount"] != System.DBNull.Value)

                        {
                            DeleteProductSpecialPrice(id);
                            decimal sleva = Math.Round(Convert.ToDecimal(dt.Rows[i]["Discount"]) / 100, 2);
                            AddProductSpecialPrice(id, 0, sleva);
                        }
                    }
                    catch (Exception ex)
                    {
                        SetChyba(" - CHYBA: " + ex.Message.ToString());
                    }
                }
            }
        }
        EndApp();
    }

    private static int UpdateProduct(int id, int web_id, int id_category, string nazev, string popis, string shortpopis, decimal cena, decimal mnozstvi, int tax_group, bool novariant)
    {
        Bukimedia.PrestaSharp.Factories.ProductFactory fct = new Bukimedia.PrestaSharp.Factories.ProductFactory(Settings.Default.baseurl, Settings.Default.account, "");
        product myprd = null;
        try
        {
            myprd = fct.Get(web_id);
        }
        catch (Exception ex)
        {
            string param = string.Format("8|{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}", 0, id, getRequeststring(nazev), getRequeststring("-"), getRequeststring(shortpopis), getRequeststring(getRewriteFromName(nazev)), Settings.Default.default_lng, tax_group);
            int prdid = GetExecuteSQLId(param);
            //bool exec_shop = ExecuteSQL(string.Format("5 {0} {1} {2} {3} {4}", prdid, reference, price.ToString().Replace(',', '.'), quantity, isdefaulton ? 1 : 0));
            if (prdid > 0)
            {
                param = string.Format("14|{0}|{1}", prdid, getRequeststring(popis));
                //bool upd = UpdateProduct(id, prdid, id_category, nazev, popis, shortpopis, cena, mnozstvi, tax_group, novariant);
                bool upd = ExecuteSQL(param);
                if (upd)
                {
                    SetLog(" - OK.");
                    return prdid;
                }
                else
                {
                    SetLog(param);
                    SetChyba(" - CHYBA");
                    return 0;
                }
            }
            else
            {
                SetLog(param);
                SetChyba(" - CHYBA");
                return 0;
            }
        }

        if (myprd != null)
        {
            //myprd.id_category_default = id_category;

            myprd.name[0].Value = nazev;

            myprd.description[0].Value = popis;
            myprd.description_short[0].Value = shortpopis;
            myprd.link_rewrite[0].Value = getRewriteFromName(nazev);
            myprd.price = 0;
            myprd.active = 1;
            if (novariant)
                myprd.price = cena;

            myprd.reference = id.ToString();
            myprd.unit_price_ratio = myprd.price;
            myprd.wholesale_price = myprd.price;
            myprd.id_tax_rules_group = tax_group;
            myprd.quantity_discount = Convert.ToInt32(mnozstvi);

            //myprd.date_upd = DateTime.Now.ToString("YYYY-MM-DD HH:MM:SS");
            myprd.position_in_category = 0;

            try
            {
                SetLog("Aktualizuji produkt.", false);
                fct.Update(myprd);
                if (myprd.reference.ToString() == id.ToString())
                {
                    SetLog(" - OK.");
                    return (int) myprd.id;
                }
                else
                {
                    string param = string.Format("8|{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}", 0, id, getRequeststring(nazev), getRequeststring("-"), getRequeststring(shortpopis), getRequeststring(getRewriteFromName(nazev)), Settings.Default.default_lng, tax_group);
                    int prdid = GetExecuteSQLId(param);
                    //bool exec_shop = ExecuteSQL(string.Format("5 {0} {1} {2} {3} {4}", prdid, reference, price.ToString().Replace(',', '.'), quantity, isdefaulton ? 1 : 0));
                    if (prdid > 0)
                    {
                        param = string.Format("14|{0}|{1}", prdid, getRequeststring(popis));
                        //bool upd = UpdateProduct(id, prdid, id_category, nazev, popis, shortpopis, cena, mnozstvi, tax_group, novariant);
                        bool upd = ExecuteSQL(param);
                        if (upd)
                        {
                            SetLog(" - OK.");
                            return prdid;
                        }
                        else
                        {
                            SetChyba(" - CHYBA");
                            return prdid;
                        }
                    }
                    else
                    {
                        SetChyba(" - CHYBA");
                        return prdid;
                    }
                }
            }
            catch (Exception ex)
            {
                string param = string.Format("8|{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}", 0, id, getRequeststring(nazev), getRequeststring(popis), getRequeststring(shortpopis), getRequeststring(getRewriteFromName(nazev)), Settings.Default.default_lng, tax_group);
                int prdid = GetExecuteSQLId(param);
                //bool exec_shop = ExecuteSQL(string.Format("5 {0} {1} {2} {3} {4}", prdid, reference, price.ToString().Replace(',', '.'), quantity, isdefaulton ? 1 : 0));
                if (prdid > 0)
                {
                    param = string.Format("14|{0}|{1}", prdid, getRequeststring(popis));
                    //bool upd = UpdateProduct(id, prdid, id_category, nazev, popis, shortpopis, cena, mnozstvi, tax_group, novariant);
                    bool upd = ExecuteSQL(param);
                    if (upd)
                    {
                        SetLog(" - OK.");
                        return prdid;
                    }
                    else
                    {
                        SetChyba(" - CHYBA");
                        return 0;
                    }
                }
                else
                {
                    SetChyba(" - CHYBA");
                    return 0;
                }
            }
        }

        return 0;
    }

    private static int UpdateProductImage(long id, long prdid, AswLib.Tblvb dt, int maxid, long prd_attr_id = 0, long varianta = 0, bool toftp = true, bool rewrite = false)
    {
        DataRow[] view = dt.Select(string.Format("Product={0}", id), "");

        if (view.Length > 0)
        {
            SetLog(string.Format("Nalezeno {0} obrázků", view.Length));
            for (int i = 0; i < view.Length; i++)
            {
                if (Convert.ToInt64(view[i]["Variant"]) != varianta)
                    continue;

                string param = string.Format("19|{0}|{1}|{2}", prdid, prd_attr_id, maxid);
                int imgid = GetExecuteSQLId(param);
                maxid = Math.Max(maxid, imgid);
                if (imgid == 0)
                {
                    param = string.Format("10|{0}|{1}|{2}|{3}", prdid, (i == 0) ? 1 : 0, Settings.Default.default_lng, prd_attr_id);
                    imgid = GetExecuteSQLId(param);
                }

                if (Convert.ToInt32(view[i]["web_id"]) > 0 && !rewrite)
                {
                    SetLog(string.Format(" - OK id: {0}, product_id: {1}, product_attribute_id: {2}, ProductImagesId: {3}", imgid, prdid, prd_attr_id, view[i]["ID"].ToString()));
                    continue;
                }

                //bool exec_shop = ExecuteSQL(string.Format("5 {0} {1} {2} {3} {4}", prdid, reference, price.ToString().Replace(',', '.'), quantity, isdefaulton ? 1 : 0));
                if (imgid > 0)
                {
                    maxid = Math.Max(maxid, imgid);
                    if (SaveImg(Convert.ToInt32(prdid), Convert.ToInt32(prd_attr_id), imgid, (byte[])view[i]["Image"], toftp))
                    {
                        view[i]["web_id"] = imgid;
                        SetLog(string.Format(" - OK id: {0}, product_id: {1}, product_attribute_id: {2}, ProductImagesId: {3}", imgid, prdid, prd_attr_id, view[i]["ID"].ToString()));
                    }
                }
                else
                {
                    SetChyba(" - CHYBA obrázek nevytvořen");
                    SetLog("");
                    SetLog(param);
                    SetLog("");
                }
            }
        }

        return maxid;
    }
    
    private static void UpdateProductsPriceNoVariantScript()
    {
        AswLib.Tblvb dt = GetProduktyNoVariant();

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            SetLog(string.Format("Product id: {0} web_id: {1}", dt.Rows[i]["id"], dt.Rows[i]["web_id"]));
            AktualizujCenuProduktu(Convert.ToInt64(dt.Rows[i]["web_id"]), Convert.ToDecimal(dt.Rows[i]["PriceNoVat"]));
        }
        EndApp();
    }

    private static void UpdateNoActiveProducts()
    {
        AswLib.Tblvb dt = GetProdukty(true,0,false,DateTime.Now,0,false,false);

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            SetLog(string.Format("Product id: {0} web_id: {1}", dt.Rows[i]["id"], dt.Rows[i]["web_id"]));
            AktivujProdukt(Convert.ToInt64(dt.Rows[i]["web_id"]), false);
        }
        

    }

    private static void UpdateNoActiveCategories()
    {
        AswLib.Tblvb dt = GetKategorie(true,0,false,false);

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            SetLog(string.Format("Kategorie id: {0} web_id: {1}", dt.Rows[i]["id"], dt.Rows[i]["web_id"]));
            AktivujKategorii(Convert.ToInt64(dt.Rows[i]["web_id"]), false);
        }


    }

    private static void UpdateNoActiveItems()
    {
        StartApp("Aktualizace neaktivních produktů a kategorií");
        UpdateNoActiveCategories();
        UpdateNoActiveProducts();
        EndApp();

    }
}