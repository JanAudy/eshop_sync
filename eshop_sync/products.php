<?php
header("Content-Type: text/html; charset=utf-8");


Function GetCnn()
{
  //$servername = "localhost:3307";
  //$username = "root";
  //$password = "MySQL55";
  //$dbname = "presta16";
  
  $servername = "wm31.wedos.net";
  $username = "a42112_shop";
  $password = "sAdm1n57!b";
  $dbname = "d42112_shop";
  
  // Create connection
  
  $myconn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($myconn->connect_error) {
      echo("Connection failed: " . $conn->connect_error);      
  }
  else
  {
      if ($myconn->set_charset("utf8")) {
        return $myconn;
      } 
  } 

}


Function CloseCnn($cn)
{
     mysqli_close($cn);
}

Function Execute($prikaz)
{
     $conn=GetCnn();
     $res = ($conn->query($prikaz) === TRUE); 
     CloseCnn($conn);
     return $res;
}

Function GetProductId($ref)
{
      
  $sql = sprintf("select ifnull(id_product,0) as id_product from ps_product where reference=%s limit 1",$ref);
  $conn=GetCnn();
  $rows=$conn->query($sql);
  $row_cnt = $rows->num_rows;
  if ($row_cnt>0)
  {
    $row=$rows->fetch_object();
    $id =$row->id_product;
    //echo $id;
    return $id;
  }
  else
    return 0;
}

Function GetProductAttributeId($ref)
{
      
  $sql = sprintf("select ifnull(id_product_attribute,0) as id_product_attribute from ps_product_attribute where reference=%s limit 1",$ref);
  $conn=GetCnn();
  $rows=$conn->query($sql);
  $row_cnt = $rows->num_rows;
  if ($row_cnt>0)
  {
    $row=$rows->fetch_object();
    $id =$row->id_product_attribute;
    //echo $id;
    return $id;
  }
  else
    return 0;
}

Function GetProductExistedImageId($id_product, $id_product_attribute, $id_image)
{


  $sql = sprintf("SELECT min(id_image) as id_image from ps_image where id_product =%s and id_image>%s",$id_product, $id_image);
  
        
  
//  echo $sql;
  $conn=GetCnn();
  $rows=$conn->query($sql);
  $row_cnt = $rows->num_rows;
  if ($row_cnt>0)
  {
    $row=$rows->fetch_object();
    $id =$row->id_image;
    //echo $id;
    if ($id >0 && $id_product_attribute >0)
    {
         $sql = sprintf("insert into ps_product_attribute_image (id_product_attribute, id_image) Values (%s,%s)", $id_product_attribute, $id);
         $res = Execute($sql);
      
    }
    return $id;
  }
  else
    return 0;
}

//

Function GetProductImageId($id_product)
{
      
  $sql = sprintf("select ifnull(max(id_image),0) as id_image from ps_image where id_product=%s limit 1",$id_product);
  $conn=GetCnn();
  $rows=$conn->query($sql);
  $row_cnt = $rows->num_rows;
  if ($row_cnt>0)
  {
    $row=$rows->fetch_object();
    $id =$row->id_image;
    //echo $id;
    return $id;
  }
  else
    return 0;
}

Function UpdateAmount($quantiy,$id_product, $id_product_attribute)
{
  $sql = sprintf("update ps_stock_available set quantity=%s where id_product=%s and id_product_attribute=%s",$quantity,$id_product, $id_product_attribute);
  return Execute($sql);
}
Function DeleteProductAssociation($id_product)
{
  $sql = sprintf("delete from ps_category_product where id_product=%s",$id_product);
  return Execute($sql);
}
Function InsertProductAssociation($id_category,$id_product, $position)
{
  $sql = sprintf("insert into ps_category_product (id_category, id_product, position) Values(%s,%s,%s)",$id_category,$id_product, $position);
  return Execute($sql);
}    

Function GetIdCategory()
{
      $sql = sprintf("select ifnull(max(id_category),0) as id_category from ps_category");
      $conn=GetCnn();
      $rows=$conn->query($sql);
      $row_cnt = $rows->num_rows;
      if ($row_cnt>0)
      {
        $row=$rows->fetch_object();
        $id =$row->id_category;
        //echo $id;
        return $id;
      }
      else
        return 0;
}

Function InsertCategory($id_category,$id_parent,$position, $root, $name, $link, $lang)
{
  $sql = sprintf("insert into ps_category (id_parent, active, date_add, date_upd, position, is_root_category) Values (%s, 1, NOW(), NOW(), %s, %s)",$id_parent, $position, $root);
  $res= Execute($sql);
  if ($res===FALSE)
  {
    echo $sql;
  }
  $id_category = GetIdCategory();
  if ($id_category>0)
  {
    $sql = sprintf("insert into ps_category_lang (id_category, id_shop, id_lang, name, description, link_rewrite, meta_title, meta_keywords, meta_description) Values (%s, 1, %s, '%s', '', '%s','','','')",$id_category, $lang, $name, $link);
    $res= ($res and Execute($sql));
    if ($res===FALSE)
    {
      echo $sql;
    }
    $sql = sprintf("insert into ps_category_shop (id_category, id_shop, position) Values (%s, 1, %s)",$id_category, $position);
    $res= ($res and Execute($sql));
    if ($res===FALSE)
    {
      echo $sql;
    }
    if($res===TRUE)
      return $id_category;
  }
  return 0;
}

Function AktivujKategorii($id_category, $active)
{
 $sql = sprintf("update ps_category set active=%s where id_category=%s", $active,$id_category);
  $res= Execute($sql);
  if ($res===FALSE)
  {
    echo $sql;
  }
 
   return $res;

}

Function AktivujProdukt($id_product, $active)
{
  $sql = sprintf("update ps_product set active=%s where id_product=%s", $active,$id_product);
  $res= Execute($sql);
  if ($res===FALSE)
  {
    echo $sql;
  }
  $sql = sprintf("update ps_product_shop set active=%s where id_product=%s", $active, $id_product);
  $res= ($res and Execute($sql));
  if ($res===FALSE)
  {
    echo $sql;
  } 
   return $res;

}

Function UpdateCategory($id_category,$id_parent,$position, $root, $name, $link, $lang)
{
  $sql = sprintf("update ps_category set active=1, id_parent=%s, position=%s, is_root_category=%s where id_category=%s",$id_parent, $position, $root, $id_category);
  $res= Execute($sql);
  if ($res===FALSE)
  {
    echo $sql;
  }
  $sql = sprintf("update ps_category_lang set name='%s', link_rewrite='%s' where id_category=%s and id_lang=%s",$name, $link, $id_category, $lang);
  $res= ($res and Execute($sql));
  if ($res===FALSE)
  {
    echo $sql;
  }
  $sql = sprintf("update ps_category_shop set position=%s where id_category=%s",$position, $id_category);
  $res= ($res and Execute($sql));
  if ($res===FALSE)
  {
    echo $sql;
  }
  return $res; 
}     

Function DeleteCategory($id_category)
{
   $sql = sprintf("delete from  ps_category where id_category=%s",$id_category);
  $res= Execute($sql);
  if ($res===FALSE)
  {
    echo $sql;
  }
  $sql = sprintf("delete from ps_category_lang where id_category=%s",$id_category);
  $res= ($res and Execute($sql));
  if ($res===FALSE)
  {
    echo $sql;
  }
  $sql = sprintf("delete from ps_category_shop where id_category=%s",$id_category);
  $res= ($res and Execute($sql));
  if ($res===FALSE)
  {
    echo $sql;
  }
  $sql = sprintf("delete from ps_category_product where id_category=%s",$id_category);
  $res= ($res and Execute($sql));
  if ($res===FALSE)
  {
    echo $sql;
  }
  return $res;

}
 
Function UpdatePrice($price,$id_product)
{
  $sql = sprintf("update ps_product_shop set price=%s where id_product=%s",$price,$id_product);
  return Execute($sql);
}

Function InsertProductAttribute($id_product, $ref, $price, $quantity, $default_on)
{
  $sql = sprintf("insert into ps_product_attribute (id_product, reference, price, quantity, default_on, supplier_reference, location, ean13, upc) Values(%s,%s,%s,%s,%s, '','','','')",$id_product, $ref, $price, $quantity, $default_on);                                                                                                      
  $res= Execute($sql);
  if ($res===TRUE)
  {
    $sql = sprintf("insert into ps_product_attribute_shop (id_product, id_product_attribute, id_shop, wholesale_price, price, ecotax, weight, unit_price_impact, default_on, minimal_quantity, available_date)  select id_product, id_product_attribute, 1, wholesale_price, price, 0, 0, 0, default_on, 0, available_date from ps_product_attribute where reference=%s",$ref);
    return Execute($sql);
  }
  else
  {
    return FALSE;
  }  
}

Function InsertProductAttributeAmmount($id_product, $id_product_attribute, $quantity)
{
  $sql = sprintf("insert into ps_stock_available (id_product, id_product_attribute, id_shop, id_shop_group, quantity, depends_on_stock, out_of_stock)  Values(%s,%s,1,0,%s,0,2)",$id_product,$id_product_attribute, $quantity);                                                                                                      
  return Execute($sql); 
}

Function UpdateSpecialPrice ($id_product, $id_product_attribute, $discount)
{
  
  $sql = sprintf("insert into ps_specific_price (id_product, id_product_attribute, price, reduction, from_quantity, reduction_tax, id_specific_price_rule, id_shop, id_shop_group, id_currency, id_customer, id_group, reduction_type)  Values(%s,%s,-1, %s,1,1,0,0,0,0,0,0,'percentage')",$id_product, $id_product_attribute, $discount);
  return Execute($sql);
  
}

Function InsertImageProduct ($id_product, $cover, $lang, $id_product_attribute)

{
  if ($cover==1)
  {
    $sql= sprintf("update ps_image set cover=null where id_product=%s",$id_product);
    $res= Execute($sql);
    $sql = sprintf("insert into ps_image (id_product, position, cover) Values (%s,1,%s)",$id_product, $cover);
  
  }
  else
  {
      $sql = sprintf("insert into ps_image (id_product, position) Values (%s,1)",$id_product);
  
  }
  $res= Execute($sql);
  if ($res===TRUE)
  {
    $id_image = GetProductImageId($id_product);
    if ($id_image>0)
    {
    
       
        
      $sql= sprintf("insert into ps_image_lang (id_image, id_lang) Values (%s,%s)",$id_image, $lang);
      
      $res= Execute($sql);
      if ($res===TRUE)
      {
         if ($cover==1)
         {
            $sql= sprintf("update ps_image_shop set cover=null where id_product=%s",$id_product);
            $res= Execute($sql);
            $sql= sprintf("insert into ps_image_shop (id_product, id_image, id_shop, cover) Values (%s,%s,1,%s)",$id_product,$id_image, $cover);
          
         } 
         else
         {
               $sql= sprintf("insert into ps_image_shop (id_product, id_image, id_shop) Values (%s,%s,1)",$id_product,$id_image);
         
         }
         $res= Execute($sql);
          if ($res===TRUE)
          {
             if($id_product_attribute>0)
             {
                $sql= sprintf("insert into ps_product_attribute_image (id_product_attribute, id_image) Values (%s,%s)",$id_product_attribute,$id_image);
                $res= Execute($sql);
              
             }
             return $id_image;
          }
          else
          {
           echo $sql;
          }
          
      } 
      else
      {
       echo $sql;
      } 
    } 
         
    
    
    
    
  }
  else
  {
   echo $sql;
    return 0;
  }
}

Function DeleteProductsCombination ($id_product)
{
 $sql= sprintf("delete from ps_product_attribute_combination where id_product_attribute In (Select id_product_attribute from ps_product_attribute where id_product =%s)",$id_product);
 $res= Execute($sql);
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_product_attribute_image where id_product_attribute In (Select id_product_attribute from ps_product_attribute where id_product =%s)",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_product_attribute_shop where id_product_attribute In (Select id_product_attribute from ps_product_attribute where id_product =%s)",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 
 $sql= sprintf("delete from ps_product_attribute where id_product =%s",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 
 return $res;

}

Function DeleteProductsUp ($id_product)
{
 $sql= sprintf("delete from ps_product_attribute_combination where id_product_attribute In (Select id_product_attribute from ps_product_attribute where id_product >%s)",$id_product);
 $res= Execute($sql);
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_product_attribute_image where id_product_attribute In (Select id_product_attribute from ps_product_attribute where id_product >%s)",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_product_attribute_shop where id_product_attribute In (Select id_product_attribute from ps_product_attribute where id_product >%s)",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_image_lang where id_image In (Select id_image from ps_image where id_product >%s)",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_image_shop where id_image In (Select id_image from ps_image where id_product >%s)",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_image where id_product >%s",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_cart_product where id_product >%s",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_stock_available where id_product >%s",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_product_attachment where id_product >%s",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_product_attribute where id_product >%s",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_product_carrier where id_product >%s",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_product_country_tax where id_product >%s",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_product_download where id_product >%s",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_product_group_reduction_cache where id_product >%s",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_product_lang where id_product >%s",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_product_sale where id_product >%s",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_product_shop where id_product >%s",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_product_supplier where id_product >%s",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_product_tag where id_product >%s",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 $sql= sprintf("delete from ps_product where id_product >%s",$id_product);
 $res= ($res and Execute($sql));
 if ($res===FALSE)
 {
  echo $sql;
 }
 return $res;

}

Function InsertProduct ($ref, $Nazev, $Popis, $Short, $Link, $Lang, $tax) {
  
    $sql= sprintf("insert into ps_product (id_supplier, id_manufacturer, id_category_default, id_shop_default, id_tax_rules_group, on_sale, online_only, ean13, upc, ecotax, quantity, minimal_quantity, price, wholesale_price, unity, unit_price_ratio, additional_shipping_cost, reference, supplier_reference, location, width, height, depth, weight, out_of_stock, quantity_discount, customizable, uploadable_files, text_fields, active, redirect_type, id_product_redirected, available_for_order, available_date, `condition`, show_price, indexed, visibility, cache_is_pack, cache_has_attachments, is_virtual, cache_default_attribute, date_add, date_upd, advanced_stock_management, pack_stock_type) Values (0, 0, 0, 1, %s, 1, 0, '', '', 0.000000, 0, 0, 0.000000, 0.000000, '', 0.000000, 0.00, %s, '', '', 0.000000, 0.000000, 0.000000, 0.000000, 2, 1, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, null, NOW(), NOW(), 0, 0)",$tax,$ref);
   // $sql= sprintf("insert into ps_product (id_supplier, id_manufacturer, id_category_default, id_shop_default, id_tax_rules_group, on_sale, online_only, ean13, upc, ecotax, quantity, minimal_quantity, price, wholesale_price, unity, unit_price_ratio, additional_shipping_cost, reference, supplier_reference, location, width, height, depth, weight, out_of_stock, quantity_discount, customizable, uploadable_files, text_fields, active, redirect_type, id_product_redirected, available_for_order, available_date, condition, show_price, indexed, visibility, cache_is_pack, cache_has_attachments, is_virtual, cache_default_attribute, date_add, date_upd, advanced_stock_management, pack_stock_type) Values (0, 0, 0, 1, 0, 1, 0, '', '', 0.000000, 0, 0, 0.000000, 0.000000, '', 0.000000, 0.00, s%, '', '', 0.000000, 0.000000, 0.000000, 0.000000, 2, 1, 0, 0, 0, 1, '', 0, 1, '0000-00-00', 'new', 1, 1, 'both', 0, 0, 0, null, NOW(), NOW(), 0, 0)
    $res= Execute($sql);
    if ($res===TRUE)
    {  
       $id_product=GetProductId($ref);
       if ($id_product>0)
       {
          $sql= sprintf("insert into ps_product_lang (id_product, id_shop, id_lang, description, description_short, link_rewrite, meta_description, meta_keywords, meta_title, name, available_now, available_later) Values (%s, 1, %s, '%s', '%s','%s', '', '', '', '%s', '', '')",$id_product,$Lang, $Popis, $Short,$Link, $Nazev );
          $res= Execute($sql);
          if ($res===TRUE)
          { 
             $sql= sprintf("insert into ps_product_shop (id_product, id_shop, id_category_default, id_tax_rules_group, on_sale, online_only, ecotax, minimal_quantity, price, wholesale_price, unity, unit_price_ratio, additional_shipping_cost, customizable, uploadable_files, text_fields, active, redirect_type, id_product_redirected, available_for_order, available_date, `condition`, show_price, indexed, visibility, cache_default_attribute, advanced_stock_management, date_add, date_upd, pack_stock_type) Values (%s, 1, 0, %s, 0, 0, 0.000000, 0, 0.000000, 0.000000, 'unity', 0.000000, 0.00, 0, 0, 0, 1, '', 0, True, '0000-00-00', 'new', 1, 1, 'both', 0, 0, NOW(), NOW(), 0)",$id_product,$tax);
            $res= Execute($sql);
                if ($res===TRUE)
                {
                   return  $id_product;
                } 
            
          }    
       }  
       
    
    }
    echo $sql;
    return 0;
}

Function UpdateProductDesc($id_product, $Popis)
{
     $sql= sprintf("update ps_product_lang set description='%s' where id_product=%s",$Popis, $id_product);     
     $res = Execute($sql);
	 $res= ($res and AktivujProdukt($id_product,1));
     if ($res===FALSE)
     {
      echo $sql;
     }
     return  $res;

}

Function UpdateProduct ($id_product, $ref, $Nazev, $Popis, $Short, $Link, $Lang, $tax)    
{
  if ($id_product==0)
  {
    $id_product=GetProductId($ref);  
  }
  if ($id_product==0)   // Produkt nen� v datab�zi - vytv���m nov� produkt
  {
      $id_product = InsertProduct ($ref, $Nazev, $Popis, $Short, $Link, $Lang, $tax);
      if ($id_product>0)
        return $id_product;
  }
  else                  // Produkt existuje v datab�zi - aktualizuji produkt  
  {
      $sql= sprintf("update ps_product_lang set description_short='%s', link_rewrite='%s', name='%s' where id_product=%s",$Short,$Link, $Nazev, $id_product);
      $res= Execute($sql);
      if ($res===TRUE)
      {
         return  $id_product;
      } 
  }
  return 0;
}

Function GetNazefFromRequest($Str)
{
        $mystr ="";
        $myvars = explode(";", $Str);
        for ($x = 0; $x < count($myvars); $x++) {
                
                $mystr.=chr($myvars[$x]);
        }
       // $mystr = mb_convert_encoding( $mystr, "WINDOWS 1250","utf-8" );
        //echo $mystr;
        //return w1250_to_utf8($mystr);
        return $mystr;
}

function w1250_to_utf8($text) {
    // map based on:
    // http://konfiguracja.c0.pl/iso02vscp1250en.html
    // http://konfiguracja.c0.pl/webpl/index_en.html#examp
    // http://www.htmlentities.com/html/entities/
    $map = array(
        chr(0x8A) => chr(0xA9),
        chr(0x8C) => chr(0xA6),
        chr(0x8D) => chr(0xAB),
        chr(0x8E) => chr(0xAE),
        chr(0x8F) => chr(0xAC),
        chr(0x9C) => chr(0xB6),
        chr(0x9D) => chr(0xBB),
        chr(0xA1) => chr(0xB7),
        chr(0xA5) => chr(0xA1),
        chr(0xBC) => chr(0xA5),
        chr(0x9F) => chr(0xBC),
        chr(0xB9) => chr(0xB1),
        chr(0x9A) => chr(0xB9),
        chr(0xBE) => chr(0xB5),
        chr(0x9E) => chr(0xBE),
        chr(0x80) => '&euro;',
        chr(0x82) => '&sbquo;',
        chr(0x84) => '&bdquo;',
        chr(0x85) => '&hellip;',
        chr(0x86) => '&dagger;',
        chr(0x87) => '&Dagger;',
        chr(0x89) => '&permil;',
        chr(0x8B) => '&lsaquo;',
        chr(0x91) => '&lsquo;',
        chr(0x92) => '&rsquo;',
        chr(0x93) => '&ldquo;',
        chr(0x94) => '&rdquo;',
        chr(0x95) => '&bull;',
        chr(0x96) => '&ndash;',
        chr(0x97) => '&mdash;',
        chr(0x99) => '&trade;',
        chr(0x9B) => '&rsquo;',
        chr(0xA6) => '&brvbar;',
        chr(0xA9) => '&copy;',
        chr(0xAB) => '&laquo;',
        chr(0xAE) => '&reg;',
        chr(0xB1) => '&plusmn;',
        chr(0xB5) => '&micro;',
        chr(0xB6) => '&para;',
        chr(0xB7) => '&middot;',
        chr(0xBB) => '&raquo;',
    );
    return html_entity_decode(mb_convert_encoding(strtr($text, $map), 'UTF-8', 'ISO-8859-2'), ENT_QUOTES, 'UTF-8');
}


Function GetIDAttrbitue ($group)  {

    $sql = sprintf("select ifnull(max(id_attribute),0) as id_attribute from ps_attribute where id_attribute_group=%s limit 1",$group);
    $conn=GetCnn();
    $rows=$conn->query($sql);
    $row_cnt = $rows->num_rows;
    if ($row_cnt>0)
    {
      $row=$rows->fetch_object();
      $id =$row->id_attribute;
      //echo $id;
      return $id;
    }
    else
      return 0;
}

Function InsertAtributValue($group, $lang, $nazev)
{
    $res = TRUE;
    $sql= sprintf("insert into ps_attribute (id_attribute_group, color, position) Values (%s, '', 0)",$group);
    $res= ($res and Execute($sql));
    if ($res===FALSE)
     {
      echo $sql;
     }
    if ($res===TRUE)
      {
         $idattr =  GetIDAttrbitue ($group);
         if ($idattr>0)
         {
            $sql= sprintf("insert into ps_attribute_lang (id_attribute, id_lang, name) Values (%s, %s, '%s')",$idattr, $lang, $nazev);
            $res= ($res and Execute($sql));
            $sql= sprintf("insert into ps_attribute_shop (id_attribute, id_shop) Values (%s, 1)",$idattr);
            $res= ($res and Execute($sql));
            if ($res===FALSE)
             {
              echo $sql;
             }
            if ($res===TRUE)
            {
                return $idattr;
            }
         }
      }
    return 0; 
}

$sql = "up";
$var  = $_GET["var"];
$vars = explode("|", $var);
//echo count($vars);
$result=FALSE;
$val =0;
if (count($vars)>0) {
    $druh =$vars[0];

    switch ($druh) {
        case 0:
            if (count($vars)==4)           // AKTUALIZACE MNO�STV�
            {
              $result = UpdateAmount($vars[1],$vars[2], $vars[3]);
              
            }
            break;
        case 1:
            if (count($vars)==2)          // ODSTRAN�N� PRODUKTU Z KATEGORIE, VAZBY
            {
              $result = DeleteProductAssociation($vars[1]);
            }
            break;
        case 2:
            
            if (count($vars)==4)         // VYTVO�EN� VAZBY PRODUKTU NA KATEGORII
            {
              $result = InsertProductAssociation($vars[1],$vars[2], $vars[3]);
            }
            break;

        case 3:
            
            if (count($vars)==3)         //AKTUALIZACE CENY PRODUKTU
            {
             $result = UpdatePrice($vars[1],$vars[2]);
            }
            break;
        case 4:
            
            if (count($vars)==6)     // VYTVO�EN� BALEN� PRODUKTU
            {
              $result = InsertProductAttribute($vars[1],$vars[2], $vars[3],$vars[4],$vars[5] );
			 

            }
            break;
        case 5:
            
            if (count($vars)==2)     // ODSTRAN�N� KOMBINAC� PRODUKTU
            {
              $result =  DeleteProductsCombination($vars[1]);            
            }
            break;
  
 
  
       
        case 6:
            
          if (count($vars)==4)        // VLO�EN� MNO�STV� PRODUKT, BALEN�
          {
             $result =InsertProductAttributeAmmount($vars[1],$vars[2], $vars[3]);
          }
          break;   
        case 7:
          if (count($vars)==4)        // AKTUALIZACE SPECI�LN� SLEVY PRODUKTU, BALEN�
          {             
            $result = UpdateSpecialPrice ($vars[1],$vars[2], $vars[3]);
           
          }    
            break;
        case 8:                     // VYTVO�EN� NEBO AKTUALIZACE PRODUKTU (n�zev, popis, apod.)
        if (count($vars)==9)
          {
            
            
            
             
            $val = UpdateProduct ($vars[1],$vars[2], GetNazefFromRequest($vars[3]), GetNazefFromRequest($vars[4]),GetNazefFromRequest($vars[5]), GetNazefFromRequest($vars[6]), $vars[7], $vars[8]);
            $result = ($val>0);
            
         
          } 

          break;
        
        case 9:                     // ODSTRAN�N� V�ECH PRODUKT� S ID > ?
        if (count($vars)==2)
        {
           $result = DeleteProductsUp ($vars[1]);
        
        }
        break;
        
        case 10:
          if (count($vars)==5)        // VLO�EN� NOV�HO OBR�ZKU
          {             
            $val  = InsertImageProduct ($vars[1],$vars[2], $vars[3], $vars[4]);
            $result = ($val>0);
          }    
            break;
        case 11:                     // ODSTRAN�N� V�ECH PRODUKT� S ID > ?
        if (count($vars)==2)
        {
           $val  = GetProductImageId($vars[1]);
           $result = ($val>0);
        
        }
        
        break;

        case 12:                     // TEST TEXT ENCODING
        if (count($vars)==2)
        {
            $str= GetNazefFromRequest($vars[1]);
            echo $str . "<br/>";
            echo w1250_to_utf8($str); 
             
            $result = TRUE;

        }
        break;

        case 13:                     // P�ID�N� NOV�HO HODNOTY ATRIBUTU
        if (count($vars)==4)
        {
            $val  = InsertAtributValue($vars[1],$vars[2], GetNazefFromRequest($vars[3]));
            $result = ($val>0);
             
            

        }
        break;
        case 14:
        if (count($vars)==3)
        {
           
          $val = UpdateProductDesc ($vars[1], GetNazefFromRequest($vars[2]));
          $result = ($val>0);
          
        } 

        break;
        case 15:    // Insert Kategorie    $id_category,$id_parent,$position, $root, $name, $link, $lang
        if (count($vars)==8)
          {
          
            $val = InsertCategory ($vars[1],$vars[2], $vars[3], $vars[4], GetNazefFromRequest($vars[5]),GetNazefFromRequest($vars[6]), $vars[7]);
            $result = ($val>0);
         
          } 

          break;
        
        case 16:    // Aktualizace Kategorie    $id_category,$id_parent,$position, $root, $name, $link, $lang
        if (count($vars)==8)
          {
          
            $result = UpdateCategory ($vars[1],$vars[2], $vars[3], $vars[4], GetNazefFromRequest($vars[5]),GetNazefFromRequest($vars[6]), $vars[7]);
            
          } 

          break;
        case 17:    // Odstran�n� Kategorie    $id_category
        if (count($vars)==2)
          {
          
            $result = DeleteCategory ($vars[1]);
            
          } 

          break;
        case 18:    // Insert Kategorie    $id_category,$id_parent,$position, $root, $name, $link, $lang
        if (count($vars)==1)
          {
          
            $val = GetIdCategory();
            $result = ($val>0);
         
          } 

          break;
        case 19:    // Hled�n� prvn�ho obr�zku dle product_id a product_attribute_id v�t�� ne�
        if (count($vars)==4)
          {
          
            $val = GetProductExistedImageId($vars[1], $vars[2], $vars[3]);
            $result = ($val>0);
            
          } 

          break;
		case 20: // Aktivace deaktivace produktu
        if (count($vars)==3)
          {
          
            $result = AktivujProdukt($vars[1], $vars[2]);
           
            
          } 

          break;
		case 21: // Aktivace deaktivace kategorie
        if (count($vars)==3)
          {
          
            $result = AktivujKategorii($vars[1], $vars[2]);
           
            
          } 

          break;
		case 22: // Get id attribute by ref
        if (count($vars)==2)
          {
          
            $val = GetProductAttributeId($vars[1]);
            $result = ($val>0);
            
          } 

          break;
        default:
            //$val = GetProductId(26650);
            $result = FALSE;
            break;    
    }

     if ($result === TRUE) 
     {
        if ($val>0)
        {
          echo $val;
        }
        else
        {
          echo "1";
        }
        
                  
     } 
     else 
     {         
        echo "0"; 
              
     }

     
 
    
}
  


   

?>